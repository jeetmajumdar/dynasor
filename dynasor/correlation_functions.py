import numpy as np
from ase import Atoms

import concurrent
from functools import partial
from itertools import combinations_with_replacement

from dynasor.logging_tools import logger
from dynasor.trajectory import Trajectory, WindowIterator
from dynasor.sample import Sample
from dynasor.post_processing import fourier_cos
from dynasor.core.time_averager import TimeAverager
from dynasor.core.reciprocal import calc_rho_q, calc_rho_j_q
from dynasor.qpoints.tools import get_index_offset


def compute_dynamic_structure_factors(
    traj: Trajectory,
    q_points: np.ndarray,
    dt: float,
    window_size: int,
    window_step: int = 1,
    calculate_currents: bool = False,
    calculate_incoherent: bool = False,
        ):
    """
    Compute dynamic structure factors
    
    Parameters
    ----------
    traj
        input Trajectory object
    q_points
        array of q points with shape ``(N_qpoints, 3)`` in Cartesian coordinates (in units of :math:`2\pi/Å`)  # noqa
    dt
        Sets the time difference between two consecutive snapshots in the trajectory to
        ``dt`` (in femtoseconds).
        Note that you should not change ``dt`` if you change ``frame_step`` in trajectory.
    window_size
        The length of the trajectory frame window to use for time correlation calculation.
        It is expressed in terms of the number of time lags to consider
        and thus determines the smallest frequency resolved.
    window_step
        Window step (or stride) is number of frames between consecutive trajectory windows.
        This does not affect time between consecutive frames in the calculation.
        If e.g. window_step > time_window, some frames will be completely unused
    calculate_currents
        Calculate the current correlations. Requires velocities to be present in trajectory.
    calculate_incoherent
        Calculate the incoherent part (self-part) of :math:`F_s`.
    """
    # sanity check input args

    if q_points.shape[1] != 3:
        raise ValueError('q-points array has wrong shape.')
    if dt <= 0:
        raise ValueError('dt must be positive.')
    if window_size <= 2:
        raise ValueError('window_size should be larger than 2.')
    if window_size % 2 != 0:
        raise ValueError('window_size should be even.')
    if window_step <= 0:
        raise ValueError('window_step must be positive.')

    # define internal parameters
    n_qpoints = q_points.shape[0]
    delta_t = traj.frame_step * dt
    N_tc = window_size + 1

    # log all setup information
    dw = 2 * np.pi / (window_size * delta_t)
    f_N = 1 / (2 * delta_t)  # Nyquist frequency
    logger.info(f'Setting dt to {dt} fs.')
    logger.info(f'Setting time between consecutive frames (dt * frame_step) to {delta_t} fs.')
    logger.info(f'Time window_size (max time lag) is {delta_t * (N_tc - 1)} fs')
    logger.info(f'Resolution omega {dw} 2pi fs^-1, max_omega  {dw * window_size} 2pi fs^-1')
    logger.info(f'Nyquist omega {f_N * 2 * np.pi} 2pi fs^-1, Nyquist frequency {f_N} fs^-1')

    if calculate_currents:
        logger.info('Calculating current (velocity) correlations')
    if calculate_incoherent:
        logger.info('Calculating self-part of correlations')

    # log some info regarding q-points
    logger.info(f'N qpoints = {n_qpoints}')

    q_directions = q_points.copy()
    q_distances = np.linalg.norm(q_points, axis=1)
    nonzero = q_distances > 0
    q_directions[nonzero] /= q_distances[nonzero].reshape(-1, 1)

    # setup functions to process frames
    def f2_rho(frame):
        rho_qs_dict = dict()
        for symbol in frame.positions_by_type.keys():
            x = frame.positions_by_type[symbol]
            rho_qs_dict[symbol] = calc_rho_q(x, q_points)
        frame.rho_qs_dict = rho_qs_dict
        return frame

    def f2_rho_and_j(frame):
        rho_qs_dict = dict()
        jz_qs_dict = dict()
        jper_qs_dict = dict()

        for symbol in frame.positions_by_type.keys():
            x = frame.positions_by_type[symbol]
            v = frame.velocities_by_type[symbol]
            rho_qs, j_qs = calc_rho_j_q(x, v, q_points)
            jz_qs = np.sum(j_qs * q_directions, axis=1)
            jper_qs = j_qs - (jz_qs[:, None] * q_directions)

            rho_qs_dict[symbol] = rho_qs
            jz_qs_dict[symbol] = jz_qs
            jper_qs_dict[symbol] = jper_qs

        frame.rho_qs_dict = rho_qs_dict
        frame.jz_qs_dict = jz_qs_dict
        frame.jper_qs_dict = jper_qs_dict
        return frame

    if calculate_currents:
        element_processor = f2_rho_and_j
    else:
        element_processor = f2_rho

    # setup window iterator
    window_iterator = WindowIterator(traj, width=N_tc, window_step=window_step,
                                     element_processor=element_processor)

    # define all pairs
    symbols = traj.particle_types
    pair_types = list(combinations_with_replacement(symbols, r=2))
    particle_counts = {key: len(val) for key, val in traj.atomic_indices.items()}
    logger.debug('Considering pairs:')
    for pair in pair_types:
        logger.debug(f'  {pair}')

    # setup all time averager instances
    F_q_t_averager = dict()
    for pair in pair_types:
        F_q_t_averager[pair] = TimeAverager(N_tc, n_qpoints)
    if calculate_currents:
        Cl_q_t_averager = dict()
        Ct_q_t_averager = dict()
        for pair in pair_types:
            Cl_q_t_averager[pair] = TimeAverager(N_tc, n_qpoints)
            Ct_q_t_averager[pair] = TimeAverager(N_tc, n_qpoints)
    if calculate_incoherent:
        F_s_q_t_averager = dict()
        for pair in symbols:
            F_s_q_t_averager[pair] = TimeAverager(N_tc, n_qpoints)

    # define correlation function
    def calc_corr(window, time_i):
        # Calculate correlations between two frames in the window without normalization 1/N
        f0 = window[0]
        fi = window[time_i]
        for s1, s2 in pair_types:
            Fqt = np.real(f0.rho_qs_dict[s1] * fi.rho_qs_dict[s2].conjugate())
            if s1 != s2:
                Fqt += np.real(f0.rho_qs_dict[s2] * fi.rho_qs_dict[s1].conjugate())
            F_q_t_averager[(s1, s2)].add_sample(time_i, Fqt)

        if calculate_currents:
            for s1, s2 in pair_types:
                Clqt = np.real(f0.jz_qs_dict[s1] * fi.jz_qs_dict[s2].conjugate())
                Ctqt = 0.5 * np.real(np.sum(f0.jper_qs_dict[s1] * fi.jper_qs_dict[s2].conjugate(), axis=1)) # noqa
                if s1 != s2:
                    Clqt += np.real(f0.jz_qs_dict[s2] * fi.jz_qs_dict[s1].conjugate())
                    Ctqt += 0.5 * np.real(np.sum(f0.jper_qs_dict[s2] * fi.jper_qs_dict[s1].conjugate(), axis=1)) # noqa

                Cl_q_t_averager[(s1, s2)].add_sample(time_i, Clqt)
                Ct_q_t_averager[(s1, s2)].add_sample(time_i, Ctqt)

        if calculate_incoherent:
            for s in symbols:
                xi = fi.positions_by_type[s]
                x0 = f0.positions_by_type[s]
                Fsqt = np.real(calc_rho_q(xi - x0, q_points))
                F_s_q_t_averager[s].add_sample(time_i, Fsqt)

    # run calculation
    with concurrent.futures.ThreadPoolExecutor() as tpe:
        # This is the "main loop" over the trajectory
        for window in window_iterator:
            logger.debug(f'processing window {window[0].frame_index} to {window[-1].frame_index}')

            # The map conviniently applies calc_corr to all time-lags. However,
            # as everything is done in place nothing gets returned so in order
            # to start and wait for the processes to finish we must iterate
            # over the None values returned
            for _ in tpe.map(partial(calc_corr, window), range(len(window))):
                pass

    # collect results into dict with numpy arrays (n_qpoints, N_tc)
    data_dict_corr = dict()
    time = delta_t * np.arange(N_tc, dtype=float)
    data_dict_corr['q_points'] = q_points
    data_dict_corr['time'] = time

    F_q_t_tot = np.zeros((n_qpoints, N_tc))
    S_q_w_tot = np.zeros((n_qpoints, N_tc))
    for pair in pair_types:
        key = '_'.join(pair)
        F_q_t = 1 / traj.n_atoms * F_q_t_averager[pair].get_average_all()
        w, S_q_w = zip(*[fourier_cos(F, delta_t) for F in F_q_t])
        w = w[0]
        S_q_w = np.array(S_q_w)
        data_dict_corr['omega'] = w
        data_dict_corr[f'F_q_t_{key}'] = F_q_t
        data_dict_corr[f'S_q_w_{key}'] = S_q_w

        # sum all partials to the total
        F_q_t_tot += F_q_t
        S_q_w_tot += S_q_w
    data_dict_corr['F_q_t_coh'] = F_q_t_tot
    data_dict_corr['S_q_w_coh'] = S_q_w_tot

    if calculate_currents:
        for pair in pair_types:
            key = '_'.join(pair)
            Cl_q_t = 1 / traj.n_atoms * Cl_q_t_averager[pair].get_average_all()
            Ct_q_t = 1 / traj.n_atoms * Ct_q_t_averager[pair].get_average_all()
            Cl_q_w = np.array([fourier_cos(C, delta_t)[1] for C in Cl_q_t])
            Ct_q_w = np.array([fourier_cos(C, delta_t)[1] for C in Ct_q_t])
            data_dict_corr[f'Cl_q_t_{key}'] = Cl_q_t
            data_dict_corr[f'Ct_q_t_{key}'] = Ct_q_t
            data_dict_corr[f'Cl_q_w_{key}'] = Cl_q_w
            data_dict_corr[f'Ct_q_w_{key}'] = Ct_q_w

    if calculate_incoherent:
        Fs_q_t_tot = np.zeros((n_qpoints, N_tc))
        Ss_q_w_tot = np.zeros((n_qpoints, N_tc))
        for symbol in symbols:
            Fs_q_t = 1 / traj.n_atoms * F_s_q_t_averager[symbol].get_average_all()
            Ss_q_w = np.array([fourier_cos(F, delta_t)[1] for F in Fs_q_t])
            data_dict_corr[f'F_s_q_t_{symbol}'] = Fs_q_t
            data_dict_corr[f'S_s_q_w_{symbol}'] = Ss_q_w

            # sum all partials to the total
            Fs_q_t_tot += Fs_q_t
            Ss_q_w_tot += Ss_q_w

        data_dict_corr['F_q_t_incoh'] = Fs_q_t_tot
        data_dict_corr['S_q_w_incoh'] = Ss_q_w_tot

    # finalize results with additional meta data
    result = Sample(data_dict_corr, symbols=symbols, pair_types=pair_types,
                    particle_counts=particle_counts, cell=traj.cell)
    return result


def compute_static_structure_factors(
    traj: Trajectory,
    q_points: np.ndarray,
        ):
    """
    Compute static structure factors

    Parameters
    ----------
    traj
        input Trajectory object
    q_points
        array of q points with shape ``(N_qpoints, 3)`` in Cartesian coordinates (in units of :math:`2\pi/Å`)  # noqa
    """
    # sanity check input args
    if q_points.shape[1] != 3:
        raise ValueError('q-points array has wrong shape.')

    n_qpoints = q_points.shape[0]
    logger.info(f'N qpoints = {n_qpoints}')

    # define all pairs
    symbols = traj.particle_types
    pair_types = list(combinations_with_replacement(symbols, r=2))
    particle_counts = {key: len(val) for key, val in traj.atomic_indices.items()}
    logger.debug('Considering pairs:')
    for pair in pair_types:
        logger.debug(f'  {pair}')

    # processing function
    def f2_rho(frame):
        rho_qs_dict = dict()
        for symbol in frame.positions_by_type.keys():
            x = frame.positions_by_type[symbol]
            rho_qs_dict[symbol] = calc_rho_q(x, q_points)
        frame.rho_qs_dict = rho_qs_dict
        return frame

    # setup averager
    Sq_averager = dict()
    for pair in pair_types:
        Sq_averager[pair] = TimeAverager(1, n_qpoints)  # time average with only timelag=0

    # main loop
    for frame in traj:
        # process_frame
        f2_rho(frame)
        logger.debug(f'processing frame {frame.frame_index}')

        for s1, s2 in pair_types:
            # compute correlation
            Sq_pair = np.real(frame.rho_qs_dict[s1] * frame.rho_qs_dict[s2].conjugate())
            if s1 != s2:
                Sq_pair += np.real(frame.rho_qs_dict[s2] * frame.rho_qs_dict[s1].conjugate())
            Sq_averager[(s1, s2)].add_sample(0, Sq_pair)

    # collect results
    data_dict = dict()
    data_dict['q_points'] = q_points
    for s1, s2 in pair_types:
        data_dict[f'S_q_{s1}_{s2}'] = 1 / traj.n_atoms * \
            Sq_averager[(s1, s2)].get_average_at_timelag(0)

    # finalize results
    result = Sample(data_dict, symbols=symbols, pair_types=pair_types,
                    particle_counts=particle_counts, cell=traj.cell)
    return result


def compute_spectral_energy_density(
    traj: Trajectory,
    ideal_supercell: Atoms,
    primitive_cell: Atoms,
    q_points: np.ndarray,
    dt: float,
        ):
    """
    Compute Spectral energy density (SED) for specific q-points.

    Note that SED is only suitable for crystalline materials without diffusion as
    atoms are assumed to move around fixed equilibrium position (defined by ``ideal``) throughout
    the enitre trajectory.

    Note that this implementation reads the full trajectory (up to max_frames),
    and can thus consume a lot of memory.

    Units of the returned SED is Da * (Å/fs)^2.

    More details can be found in
    Thomas *et al.* (2010),
    *Predicting phonon dispersion relations and lifetimes from the spectral energy density*,
    Physical Review B **81**, 081411 (2010); https://doi.org/10.1103/physrevb.81.081411

    Parameters
    ----------
    traj
        input Trajectory object
    ideal_supercell
        ideal atoms object
    primitive_cell
        compatible primitive cell. Must be aligned correctly with ``ideal``
    q_points
        array of q points with shape ``(N_qpoints, 3)`` in Cartesian coordinates (in units of :math:`2\pi/Å`)  # noqa
    dt
        Sets the time difference between two consecutive snapshots in the trajectory to
        ``dt`` (in femtoseconds).
        Note that you should not change ``dt`` if you change ``frame_step`` in trajectory.

    Returns
    -------
    w
        Frequencies in an array of length ``n_times`` in units of :math:`2\pi/fs`  # noqa
    sed
        array of shape ``(n_qpoints, n_times)``
    """

    delta_t = traj.frame_step * dt

    # logger
    logger.info('Running SED')
    logger.info(f'Setting time between consecutive frames (dt * step) to {delta_t} fs.')

    # check that the ideal supercell agrees with traj
    if traj.n_atoms != len(ideal_supercell):
        raise ValueError('ideal_supercell must contain the same number of atoms as the trajectory.')

    # colllect all velocities
    velocities = []
    for it, frame in enumerate(traj):
        logger.debug(f'Reading frame {it}')
        if frame.velocities_by_type is None:
            raise ValueError(f'Could not read velocities from frame {it}')
        v = frame.get_velocities_as_array(traj.atomic_indices)
        velocities.append(v)

    velocities = np.array(velocities)
    velocities = velocities.transpose(1, 2, 0).copy()
    velocities = np.fft.fft(velocities, axis=2)

    # calculate SED
    masses = primitive_cell.get_masses()
    indices, offsets = get_index_offset(ideal_supercell, primitive_cell)

    pos = np.dot(q_points, np.dot(offsets, primitive_cell.cell).T)
    exppos = np.exp(1.0j * pos)
    density = np.zeros((len(q_points), velocities.shape[2]))
    for alpha in range(3):
        for b in range(len(masses)):
            tmp = np.zeros(density.shape, dtype=complex)
            for i in range(len(indices)):
                index = indices[i]
                if index != b:
                    continue
                tmp += np.outer(exppos[:, i], velocities[i, alpha])

            density += masses[b] * np.abs(tmp)**2

    # frequencies
    w = np.linspace(0.0, 2 * np.pi / delta_t, density.shape[1])  # units of 2pi/fs

    return w, density
