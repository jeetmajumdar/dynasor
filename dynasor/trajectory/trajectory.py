__all__ = ['Trajectory', 'WindowIterator']

import numpy as np

from collections import deque
from itertools import islice, chain
from os.path import isfile
from typing import Callable

from dynasor.trajectory.atomic_indices import parse_gromacs_index_file
from dynasor.trajectory.ase_trajectory_reader import ASETrajectoryReader
from dynasor.trajectory.extxyz_trajectory_reader import ExtxyzTrajectoryReader
from dynasor.trajectory.lammps_trajectory_reader import LammpsTrajectoryReader
from dynasor.trajectory.mdanalysis_trajectory_reader import MDAnalysisTrajectoryReader
from dynasor.trajectory.trajectory_frame import TrajectoryFrame
from dynasor.logging_tools import logger


class Trajectory:
    """
    dynasor-style trajectory iterator class,
    i.e. imple wrapper for the trajectory_reader-classes.

    Each iterator step returns a Frame object, which holds positions by atom types,
    and possibly velocties by atom types.


    Parameters
    ----------
    filename
        name of input file
    trajectory_format
        Type of trajectory. Choose from: 'lammps_internal', 'extxyz',
        or one of the formats supported by MDAnalysis (except 'lammpsdump',
        which is called through 'lammps_mdanalysis' to avoid ambiguity)
    atomic_indices
        Specify which atomic indices belong to what atom-types (either dict or index-file)
    length_unit
        Length unit of trajectory. Necessary for correct conversion to internal dynasor units if
        the trajectory file does not contain unit information. For available options see
        MDAnalysis.
    time_unit
        Time unit of trajectory. Necessary for correct conversion to internal dynasor units if
        the trajectory file does not contain unit information. For available options see
        MDAnalysis.
    frame_start
        First frame to read; must be >= 0.
    frame_stop
        Last frame to read. Default None means reading the entire trajectory.
    frame_step
        step to access (1 by default = every single frame); must be > 0.
    """
    def __init__(self,
                 filename: str,
                 trajectory_format: str,
                 atomic_indices=None,
                 length_unit: str = None,
                 time_unit: str = None,
                 frame_start: int = 0,
                 frame_stop: int = None,
                 frame_step: int = 1):

        if frame_start < 0:
            raise ValueError('frame_start should be positive')
        if frame_step < 0:
            raise ValueError('frame_step should be positive')

        self._frame_number = frame_start
        self._frame_step = frame_step
        self._frame_stop = frame_stop

        # setup trajectory reader
        if not isfile(filename):
            raise IOError(f'File {filename} does not exist')
        self._filename = filename

        if trajectory_format == 'lammps_internal':
            reader = LammpsTrajectoryReader
        elif trajectory_format == 'extxyz':
            reader = ExtxyzTrajectoryReader
        elif trajectory_format == 'lammps_mdanalysis':
            reader = MDAnalysisTrajectoryReader
            trajectory_format = 'lammpsdump'
        elif trajectory_format == 'ase':
            reader = ASETrajectoryReader
        elif trajectory_format == 'lammps':
            raise IOError('Ambiguous trajectory format, '
                          'did you mean lammps_internal or lammps_mdanalysis?')
        else:
            reader = MDAnalysisTrajectoryReader

        logger.debug(f'Using trajectory reader: {reader.__name__}')
        if reader == MDAnalysisTrajectoryReader:
            self._reader_obj = reader(self._filename, trajectory_format,
                                      length_unit=length_unit, time_unit=time_unit)
        else:
            self._reader_obj = reader(self._filename, length_unit=length_unit, time_unit=time_unit)

        # Get two frames to set cell etc.
        frame0 = next(self._reader_obj)
        frame1 = next(self._reader_obj)
        self._cell = frame0.cell
        self._n_atoms = frame0.n_atoms

        # Make sure cell is not changed during consecutive frames
        if not np.allclose(frame0.cell, frame1.cell):
            raise ValueError('The cell changes between the first and second frame. '
                             'The concept of q-points becomes muddy if the simulation cell is '
                             'changing, such as during NPT MD simulations, so trajectories where '
                             'the cell changes are not supported by dynasor.')

        # setup iterator slice (reuse frame0 and frame1 via chain)
        self._reader_obj = islice(chain([frame0, frame1], self._reader_obj),
                                  frame_start, self._frame_stop, self._frame_step)

        # setup atomic indices
        if atomic_indices is None:  # Default behaviour
            atomic_indices = {'X': np.arange(0, self.n_atoms)}
        elif isinstance(atomic_indices, str):  # Str input
            if atomic_indices == 'read_from_trajectory':
                atomic_indices = frame0['atom_types']
            else:
                atomic_indices = parse_gromacs_index_file(atomic_indices)
        elif isinstance(atomic_indices, dict):  # Dict input
            pass
        else:
            raise ValueError('Could not understand atomic_indices.')
        self._atomic_indices = atomic_indices

        # sanity checks for atomic_indices
        for key, indices in self._atomic_indices.items():
            if np.max(indices) > self.n_atoms:
                raise ValueError('maximum index in atomic_indices exceeds number of atoms')
            if np.min(indices) < 0:
                raise ValueError('minimum index in atomic_indices is negative')
            if '_' in key:
                # Since '_' is the symbol we use to in results, e.g. Sqw_Cs_Pb
                raise ValueError('The char "_" is not allowed in atomic_indices.')

        # log info on trajectory and particles types etc
        self.particle_types = sorted(self._atomic_indices.keys())
        logger.info(f'Trajectory file: {self._filename}')
        logger.info(f'With a total of {self._n_atoms} particles, {len(self.particle_types)} types.')
        for symbol, indices in self._atomic_indices.items():
            logger.info(f'{symbol}: {len(indices)}')
        logger.info(f'Simulation cell is (Angstrom):\n{str(self._cell)}')

    def __iter__(self):
        return self

    def __next__(self):
        frame = next(self._reader_obj)
        new_frame = TrajectoryFrame(self.atomic_indices, frame.frame_index, frame.positions,
                                    frame.velocities)
        return new_frame

    @property
    def cell(self):
        """ Simulation cell """
        return self._cell

    @property
    def n_atoms(self):
        """ Number of atoms """
        return self._n_atoms

    @property
    def filename(self):
        """ The trajectory filename """
        return self._filename

    @property
    def atomic_indices(self):
        """ Return copy of index arrays """
        atomic_indices = dict()
        for name, inds in self._atomic_indices.items():
            atomic_indices[name] = inds.copy()
        return atomic_indices

    @property
    def frame_step(self):
        """ step to access, trajectory will return every frame step snapshot """
        return self._frame_step


def consume(iterator, n):
    """ Advance the iterator by n steps. If n is none, consume entirely. """
    # From the python.org
    if n is None:
        deque(iterator, maxlen=0)
    else:
        next(islice(iterator, n, n), None)


class WindowIterator:
    """Sliding window iterator.

    Returns consecutive windows (a windows is represented as a list
    of objects), created from an input iterator.

    Parameters
    ----------
    itraj
        Trajectory object
    width
        Length of window (``window_size`` + 1)
    window_step
        Distance between the start of two consecutive window frames
    element_processor
        Enables processing each non-discarded object; useful if ``window_step >
        width`` and ``map_item`` is expensive (as compared to directly passing
        ``map(fun, itraj)`` as ``itraj``); if ``window_step < width``, you could as
        well directly pass ``map(fun, itraj)``.
    """
    def __init__(self,
                 itraj: Trajectory,
                 width: int,
                 window_step: int = 1,
                 element_processor: Callable = None):

        self._raw_it = itraj
        if element_processor:
            self._it = map(element_processor, self._raw_it)
        else:
            self._it = self._raw_it
        assert window_step >= 1
        assert width >= 1
        self.width = width
        self.window_step = window_step
        self._window = None

    def __iter__(self):
        return self

    def __next__(self):
        """ Returns next element in sequence. """
        if self._window is None:
            self._window = deque(islice(self._it, self.width), self.width)
        else:
            if self.window_step >= self.width:
                self._window.clear()
                consume(self._raw_it, self.window_step - self.width)
            else:
                for _ in range(min((self.window_step, len(self._window)))):
                    self._window.popleft()
            for f in islice(self._it, min((self.window_step, self.width))):
                self._window.append(f)

        if len(self._window) == 0:
            raise StopIteration

        return list(self._window)
