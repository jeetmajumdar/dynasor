from dataclasses import dataclass
import numpy as np
from typing import Dict


@dataclass
class ReaderFrame:
    """Trivial data struct holding MD-data for one time frame

    Parameters
    ----------
    frame_index
        trajectory index of the snapshot (frame)
    cell
        simulation cell as 3 row vectors (Å)
    n_atoms
        number of atoms
    positions
        particle positions as 3xn_atoms array (Å)
    velocities
        particle velocities as 3xn_atoms array (Å/fs);
        may not be available, depending on reader and trajectory file format
    """
    frame_index: int
    cell: np.ndarray
    n_atoms: int
    positions: np.ndarray
    velocities: np.ndarray = None


class TrajectoryFrame:
    """
    Class holding positions and optionally velocities split by atom types
    for one snapshot (frame) in a trajectory

    Attributes
    ----------
    * positions_by_type
    * velocities_by_type

    such that e.g.
    positions_by_type['Cs'] numpy array with shape (n_atoms_Cs, 3)
    positions_by_type['Pb'] numpy array with shape (n_atoms_Pb, 3)

    Parameters
    ----------
    atomic_indices
        dictionary with keys as atomic species and values as atomic indices
    frame_index
        trajectory index of the snapshot (frame)
    positions
        atomic positions as an array with shape ``(n_atoms, 3)``
    velocities
        atomic velocities as an array with shape ``(n_atoms, 3)``; defaults to ``None``
    """

    def __init__(self,
                 atomic_indices: Dict[str, int],
                 frame_index: int,
                 positions: np.ndarray,
                 velocities: np.ndarray = None):
        self.frame_index = frame_index

        self.positions_by_type = dict()
        for symbol, indices in atomic_indices.items():
            self.positions_by_type[symbol] = positions[indices, :].copy()

        if velocities is not None:
            self.velocities_by_type = dict()
            for symbol, indices in atomic_indices.items():
                self.velocities_by_type[symbol] = velocities[indices, :].copy()
        else:
            self.velocities_by_type = None

    def get_positions_as_array(self, atomic_indices: Dict[str, list]):
        """
        Construct the full positions array with shape ``(n_atoms, 3)``.

        Parameters
        ---------
        atomic_indices
            dictionary with keys as atomic species and values as atomic indices
        """

        # check that atomic_indices is complete
        n_atoms = np.max([np.max(indices) for indices in atomic_indices.values()]) + 1
        all_inds = [i for indices in atomic_indices.values() for i in indices]
        if len(all_inds) != n_atoms or len(set(all_inds)) != n_atoms:
            raise ValueError('atomic_indices is incomplete')

        # collect positions into a single array
        x = np.empty((n_atoms, 3))
        for atom_type, indices in atomic_indices.items():
            x[indices, :] = self.positions_by_type[atom_type]
        return x

    def get_velocities_as_array(self, atomic_indices: Dict[str, list]):
        """
        Construct the full velocities array with shape ``(n_atoms, 3)``.

        Parameters
        ---------
        atomic_indices
            dictionary with keys as atomic species and values as atomic indices
        """

        # check that atomic_indices is complete
        n_atoms = np.max([np.max(indices) for indices in atomic_indices.values()]) + 1
        all_inds = [i for indices in atomic_indices.values() for i in indices]
        if len(all_inds) != n_atoms or len(set(all_inds)) != n_atoms:
            raise ValueError('atomic_indices is incomplete')

        # collect velocities into a single array
        v = np.empty((n_atoms, 3))
        for atom_type, indices in atomic_indices.items():
            v[indices, :] = self.velocities_by_type[atom_type]
        return v

    def __str__(self):

        s = [f'Frame index {self.frame_index}']
        for key, val in self.positions_by_type.items():
            s.append(f'  positions {key} shape {val.shape}')
        if self.velocities_by_type is not None:
            for key, val in self.velocities_by_type.items():
                s.append(f'  velocities {key} shape {val.shape}')
        return '\n'.join(s)
