import itertools
import numpy as np
from numpy.typing import NDArray
from dynasor.logging_tools import logger


def get_prune_distance(max_points, q_max, q_vol):
    """Return the prune distance for q-points in the isotropic case

    max_points corresponds to the wanted number of resulting q-points,
    max_q corresponds to the maximum q-value in the resulting q-point set,
    vol_q corresponds to the q-space volume for a single q-point.
    If points are selected from the full grid with probability
    min(1, (q_prune/|q|)**2), q-space will on average be sampled with
    an equal number of points per radial unit (for q > q_prune).

    The general idea is as follows:

    We know that the number of q-points within a radius Q is given by

    n = 1/v int_0^Q dq 4pi q**2 = 1/v 4/3 pi Q**3

    where v is the volume of one q-point. Now we want to find a distance P such
    that if all points outside this radius is weighted by the function w(q) the
    total number of q-points will equal the target N while the number of
    q-points increase linearly from P outward. One additional constraint is
    that the weighting function must be 1 at P. The weighting function which
    accomplishes this is w(q)=P^2/q^2

    N = 1/v int_0**P 4pi q**2 + 1/v int_P**Q 4pi q**2 P**2/q**2 dq

    This results in a third order equation for P and is what is solved for

    See e.g. https://en.wikipedia.org/wiki/Cubic_equation

    """

    Q = q_max
    V = q_vol
    N = max_points

    # Coefs
    a = 1.0
    b = -3/2*Q
    c = 0.0
    d = 3/2 * V * N / (4*np.pi)

    # Eq tol solve
    def original_eq(x):
        return a * x**3 + b * x**2 + c * x + d
    # original_eq = lambda x:  a * x**3 + b * x**2 + c * x + d

    # Discriminant
    p = (3*a*c - b**2) / (3*a**2)
    q = (2*b**3 - 9*a*b*c + 27*a**2*d) / (27*a**3)

    D_t = - (4*p**3 + 27*q**2)
    if D_t < 0:
        return q_max

    x = Q * (np.cos(1/3 * np.arccos(1 - 4*d / Q**3) - 2 * np.pi / 3) + 0.5)

    assert np.isclose(original_eq(x), 0), original_eq(x)

    return x


def calc_solid_angle(cell: NDArray):
    # from https://en.wikipedia.org/wiki/Solid_angle#Tetrahedron
    assert np.linalg.det(cell) > 0

    A, B, C = cell  # cell vectors
    a, b, c = np.linalg.norm(cell, axis=1)  # cell vector lengths

    ac = np.arccos(A @ B / a / b)  # angle a-o-b
    ab = np.arccos(A @ C / a / c)  # angle b-o-c
    aa = np.arccos(B @ C / b / c)  # angle b-o-c

    a = (aa + ab + ac) / 2

    tan_sigma_over_4 = np.sqrt(np.tan(a/2) * np.tan((a-aa)/2) * np.tan((a-ab)/2) * np.tan((a-ac)/2))

    sigma = np.arctan(tan_sigma_over_4) * 4

    return sigma


def generate_spherical_qpoints(cell: NDArray, q_max: float, max_points: int = None, seed: int = 42):
    """Generates all q-points in the first octant of the inverse lattice within
    a given radius q_max.
    Suitable for isotropic sampling of q-space

    If number of q-points generated exceed ``max_points``, then points will randomly be pruned
    If the number of generated q-points exceeds ``max_points``, then q-points will be randomly
    removed in such a way that the number of q-points inside roughly uniformly distributed
    with respect to |q|.

    Parameters
    ----------
    cell
        real cell with cell vectors as rows
    q_max
        maximum length of generated q-points
    max_points
        Optionally limit the set to approximately max_points points by randomly
        removing points from a "fully populated grid". The points are removed
        in such a way that for q > q_prune, the points will be radially
        uniformely distributed (the value of q_prune is calculated from max_q,
        max_points, and the shape of the cell).
    seed
        Seed used for the stochastic pruning.

    Returns
    -------
    qpoints
        q-points in cartesian coordinates as a Nx3 array

    """

    # inv(A.T) == inv(A).T
    # The physicists reciprocal cell
    rec_cell = np.linalg.inv(cell.T) * 2*np.pi

    # We want to find all points on the lattice defined by the reciprocal cell
    # such that all points within max_q are in this set
    inv_rec_cell = np.linalg.inv(rec_cell.T)  # cell / 2pi

    # h is the height of the rec_cell perpendicular to the other two vectors
    h = 1 / np.linalg.norm(inv_rec_cell, axis=1)

    # If a q_point has a coordinate larger than this number it must be further away than q_max
    N = np.ceil(q_max / h).astype(int)

    # Create q-points in the first octant
    lattice_points = list(itertools.product(*[range(n+1) for n in N]))
    q_points = lattice_points @ rec_cell

    # Calculate distances for pruning
    q_distances = np.linalg.norm(q_points, axis=1)  # Find distances

    # Sort distances and q-points based on distance
    argsort = np.argsort(q_distances)
    q_distances = q_distances[argsort]
    q_points = q_points[argsort]

    # Prune based on distances
    q_points = q_points[q_distances <= q_max]
    q_distances = q_distances[q_distances <= q_max]

    # Pruning based on max_points
    if max_points is not None and max_points < len(q_points):

        solid_angle = calc_solid_angle(rec_cell)
        angle_factor = solid_angle / (4*np.pi)

        q_vol = np.linalg.det(rec_cell)

        q_prune = get_prune_distance(max_points / angle_factor, q_max, q_vol)

        if q_prune < q_max:
            logger.info(f'Pruning at {q_prune:.3} < {q_max}')

            # Keep point with probability min(1, (q_prune/|q|)^2) ->
            # aim for an equal number of points per equally thick "onion peel"
            # to get equal number of points per radial unit.
            p = np.ones(len(q_points))
            assert np.isclose(q_distances[0], 0)
            p[1:] = (q_prune / q_distances[1:]) ** 2

            rs = np.random.RandomState(seed)
            q_points = q_points[p > rs.rand(len(q_points))]

            logger.info(f'Pruned from {len(q_distances)} q-points to {len(q_points)}')

    return q_points
