from dynasor.qpoints.spherical_qpoints import generate_spherical_qpoints


__all__ = ['generate_spherical_qpoints']
