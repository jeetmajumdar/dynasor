# -*- coding: utf-8 -*-

"""
dynasor module.
"""

__project__ = 'dynasor'
__description__ = 'A tool for calculating dynamic structure factors'
__copyright__ = '2023'
__license__ = 'MIT'
__credits__ = ['The dynasor developers team']
__version__ = '1.2'
__maintainer__ = 'The dynasor developers team'
__maintainer_email__ = 'dynasor@materialsmodeling.org'
__status__ = 'Development Status :: 5 - Production/Stable'
__url__ = 'http://dynasor.materialsmodeling.org/'
