from typing import Dict, List

import numpy as np
from numpy.typing import NDArray


class Sample:
    """
    Class for holding correlation functions and some additional meta data.
    Sample objects can be written to and read from file.

    Parameters
    ----------
    data_dict
        Dictionary with correlation functions.
    symbols
        Atomic symbols.
    pair_types
        Pairs for which the correlation functions have been calculated.
    particle_counts
        Number of particles of each species.
    cell
        Simulation cell.
    meta_data
        Dictionary with any additional meta data, for example time stamps, user names, etc.
    """

    def __init__(self, data_dict: Dict, symbols: List[str] = None, pair_types: List = None,
                 particle_counts: Dict[str, int] = None, cell: NDArray[np.float64] = None,
                 **meta_data: Dict):
        self._initial_data_keys = list(data_dict)
        for key in data_dict:
            setattr(self, key, data_dict[key])
        self._symbols = symbols
        self._pair_types = pair_types
        self._particle_counts = particle_counts
        self._cell = cell
        if meta_data is not None:
            self._meta_data_keys = set(meta_data)
            for key in meta_data:
                if key in ['symbols', 'pair_types',
                           'particle_counts', 'cell'] + self._initial_data_keys:
                    raise ValueError(f'meta_data contains duplicate of {key}. Rename or '
                                     'remove this entry.')
                else:
                    setattr(self, key, meta_data[key])
        else:
            self._meta_data_keys = []

    def __getitem__(self, key):
        """ Makes it possible to get the attributes using Sample['key'] """
        try:
            return getattr(self, key)
        except AttributeError:
            raise KeyError(key)

    def write_to_npz(self, fname: str):
        """ Write object to file in numpys npz format.

        Parameters
        ----------
        fname
            Name of the file in which to store the Sample object.
        """
        data_to_save = dict()
        meta_data_dict = dict(symbols=self.symbols, particle_counts=self.particle_counts,
                              pair_types=self.pair_types, cell=self.cell)
        for key in self._meta_data_keys:
            meta_data_dict[key] = getattr(self, key)
        data_to_save['meta_data'] = meta_data_dict

        initial_data_dict = dict()
        for key in self._initial_data_keys:
            initial_data_dict[key] = getattr(self, key)
        data_to_save['data_dict'] = initial_data_dict

        np.savez_compressed(fname, **data_to_save)

    @property
    def available_correlation_functions(self):
        keys_to_skip = set(['q_points', 'q_norms', 'time', 'omega'])
        return sorted(list(set(self._initial_data_keys) - keys_to_skip))

    @property
    def symbols(self):
        if self._symbols is None:
            return None
        return self._symbols.copy()

    @property
    def particle_counts(self):
        if self._particle_counts is None:
            return None
        return self._particle_counts.copy()

    @property
    def pair_types(self):
        if self._pair_types is None:
            return None
        return self._pair_types.copy()

    @property
    def cell(self):
        if self._cell is None:
            return None
        return self._cell.copy()

    def __repr__(self):
        return str(self)

    def __str__(self):
        s_contents = ['Correlation function data:']
        s_contents.append(f'Atom types: {self.symbols}')
        s_contents.append(f'Pair types: {self.pair_types}')
        s_contents.append(f'Particle counts: {self.particle_counts}')
        s_contents.append('Simulations cell:')
        s_contents.append(f'{self.cell}')
        for key in self._initial_data_keys:
            s_i = f'{key:12} with shape: {np.shape(getattr(self,key))}'
            s_contents.append(s_i)
        s = '\n'.join(s_contents)
        return s


def read_sample_from_npz(fname: str):
    """ Create object from file.

    Parameters
    ----------
    fname
        Name of the file (numpy npz format) from which to read the Sample object.
    """
    data_read = np.load(fname, allow_pickle=True)
    data_dict = data_read['data_dict'].item()
    meta_data = data_read['meta_data'].item()
    return Sample(data_dict, symbols=meta_data['symbols'], pair_types=meta_data['pair_types'],
                  particle_counts=meta_data['particle_counts'], cell=meta_data['cell'],
                  meta_data=meta_data)
