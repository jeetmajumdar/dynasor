from typing import Dict, Optional
from warnings import warn

import numpy as np

from dynasor.sample import Sample


def neutron_cross_sections(
    sample: Sample,
    coherent_scattering_lengths: Dict[str, float],
    incoherent_scattering_lengths: Optional[Dict[str, float]] = None,
) -> Sample:
    """Weights the partial dynamical structure factors in Sample by the appropriate neutron
    scattering lengths. For example, given the structure factor :math:`S_{AB}` computed for two
    species :math:`A` and :math:`B`, the weighted dynamical structure factor will be
    :math:`S'_{AB} = b_A b_B S_{AB}`.
    The total dynamical structure factor for the system can then be obtained by summing the partial
    structure factors, :math:`S = \\sum_{A, B} S'_{AB}`.
    Note that it is assumed that the partial dynamical structure factors are normalized by
    :math:`1/N`, where :math:`N` is the total number of particles in the system.

    If the :program:`dynasor` analysis has been run without calculating the (incoherent) self-part,
    the total dynamic structure factor returned by this function will only consist of the coherent
    part. In this case, it is not necessary to supply :attr:`incoherent_scattering_lengths`.

    Parameters
    ----------
    sample
        :program:`dynasor` output sample.
    coherent_scattering_lengths
        A dict with keys and values representing the atomic species and their corresponding
        coherent scattering length, ``{'A': b_A }``.
    incoherent_scattering_lengths
        A dict with keys and values representing the atomic species and their corresponding
        incoherent scattering length, ``{'A': b_A }``.

    Returns
    -------
       A :class:`Sample` instance with the weighted partial structure factors.
    """
    # Only the intermediate and dynamic structure factors, without q_points, time and omega
    factors = {}

    all_species = []
    for key in sample.available_correlation_functions:
        if 'coh' in key:
            continue
        split_key = key.split('_')
        # If this is a self-factor, the species will only appear once in the end.
        # Else, grab the last two characters.
        if split_key[1] == 's':
            species = split_key[-1] * 2
        else:
            species = split_key[3:]
        all_species.extend(species)
        factors[key] = {'data': getattr(sample, key), 'species': species}
    species_in_sample = np.unique(all_species)

    # Check if self-part has been computed
    has_self_correlations = np.any([key.split('_')[1] == 's' for key in factors.keys()])
    if not has_self_correlations:
        warn(
            (
                'No self-correlations found. Weighted structure '
                'factors will only contain the coherent part.'
            ),
            UserWarning,
        )
    elif has_self_correlations and incoherent_scattering_lengths is None:
        raise ValueError(
            'incoherent_scattering_lengths is required when self-part is present'
        )

    # Check if coherent scattering lengths are valid
    coherent_species = set(coherent_scattering_lengths.keys())
    if not len(coherent_species) == len(species_in_sample):
        raise ValueError(
            'Number of species in coherent_scattering_lengths does not match sample'
        )
    if not coherent_species == set(species_in_sample):
        missing_sets = coherent_species.difference(set(species_in_sample))
        raise ValueError(f'Species {missing_sets} not found in sample')

    if incoherent_scattering_lengths is not None:
        # Check if incoherent_scattering_lengths are valid
        incoherent_species = set(incoherent_scattering_lengths.keys())
        if not coherent_species == incoherent_species:
            raise ValueError('Species for coherent and incoherent does not match')

    # In the following we assume that all the partial structure factors
    # are weighted by 1/N.
    weighted_sample = {}
    for key, factor in factors.items():
        data, species = factor['data'], factor['species']

        is_self_part = key.split('_')[1] == 's'
        scattering_lengths = (
            incoherent_scattering_lengths
            if is_self_part
            else coherent_scattering_lengths
        )

        weight = np.prod([scattering_lengths[s] for s in species])
        weighted_data = data * weight  # S_AB * b_A * b_B
        weighted_sample[key] = weighted_data

        sum_key = 'S_q_w' if key.startswith('S') else 'F_q_t'
        sum_key += '_incoh' if is_self_part else '_coh'

        if sum_key not in weighted_sample.keys():
            weighted_sample[sum_key] = np.zeros(data.shape)
        weighted_sample[sum_key] += weighted_data

    # Sum up to get total dynamical structure factor
    if 'S_q_w_coh' in weighted_sample.keys():
        weighted_sample['S_q_w'] = np.zeros(weighted_sample['S_q_w_coh'].shape)
        weighted_sample['S_q_w'] += weighted_sample['S_q_w_coh']
        if has_self_correlations:
            weighted_sample['S_q_w'] += weighted_sample['S_q_w_incoh']

    if 'F_q_t_coh' in weighted_sample.keys():
        weighted_sample['F_q_t'] = np.zeros(weighted_sample['F_q_t_coh'].shape)
        weighted_sample['F_q_t'] += weighted_sample['F_q_t_coh']
        if has_self_correlations:
            weighted_sample['F_q_t'] += weighted_sample['F_q_t_incoh']

    # Add back q_points, omega and time information
    for key in sample._initial_data_keys:
        if key not in weighted_sample.keys():
            weighted_sample[key] = getattr(sample, key)

    # Add metadata
    new_meta_data = {key: sample[key] for key in sample._meta_data_keys.copy()}
    return Sample(data_dict=weighted_sample,
                  symbols=sample.symbols,
                  particle_counts=sample.particle_counts,
                  pair_types=sample.symbols,
                  cell=sample.cell,
                  **new_meta_data
                  )


def xray_cross_sections(sample):
    pass


def electron_cross_sections(sample):
    pass
