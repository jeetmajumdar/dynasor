## Static and dynamic properties of liquid aluminium
Here, two example notebooks are provided to demonstrate how to compute static and dynamic properties with `dynasor`.

First the MD trajectory must be generated which is done via lammps.
The potential file and a input script to lammps can be found in `lammpsrun`.
Run (or equvivalent for your lammps installation)

`./lmp_serial < main.in`

which generates a dump file.