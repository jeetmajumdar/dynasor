#!/bin/sh

DYNASOR=dynasor

Q_BINS=100			# Number of bins spaced evenly between Gamma and the specified q-point.
Q_POINTS=100		# Number of qpoints spaced evenly between Gamma and the specified q-point.
TIME_WINDOW=2000    # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=20000   	# Read at most MAX_FRAMES frames from trajectory file (then stop)

dt=$((5*5)) # This needs to be correspond to lammps timestep * dumpFreq * $STEP in fs in order to get the units correct.


# Q - points for T=300 
# 
# L point  q = 7.62,7.62,7.62
# K point  q = 11.43,11.43,0
#



TRAJECTORY="lammpsrun/dump/dumpT300.NVT.atom.velocity"

OUTPUT="outputs/dynasor_outT300_GK"
echo "\nRunning Gamma to K\n\n"

${DYNASOR} -f "$TRAJECTORY" \
	--q-bins=$Q_BINS \
	--q-points=$Q_POINTS \
	--max-frames=$MAX_FRAMES \
    --om=$OUTPUT.m \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--q-sampling="line" \
	--q-direction=11.43,11.43,0


OUTPUT="outputs/dynasor_outT300_GL"
echo "\nRunning Gamma to L\n\n"

${DYNASOR} -f "$TRAJECTORY" \
	--q-bins=$Q_BINS \
	--q-points=$Q_POINTS \
	--max-frames=$MAX_FRAMES \
    --om=$OUTPUT.m \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--q-sampling="line" \
	--q-direction=7.62,7.62,7.62



