import os

import numpy as np
import pytest

from dynasor.sample import Sample

N_qpoints = 20
window_size = 100


@pytest.fixture
def q_points():
    return np.array([np.array([1, 0, 0]) * amp for amp in np.linspace(0, 1, N_qpoints)])


@pytest.fixture
def time():
    return np.linspace(0, 10, window_size)


@pytest.fixture
def omega():
    return np.linspace(0, 5, window_size)


@pytest.fixture
def traj_fname_xyz_long():
    this_dir = os.path.dirname(__file__)
    traj_fname = os.path.join(this_dir,
                              'trajectory_reader/trajectory_files/dump_long_with_velocities.xyz')
    return traj_fname


@pytest.fixture
def data_dict(q_points, time, omega):
    # setup data dict
    data_dict = dict()
    data_dict['q_points'] = q_points
    data_dict['time'] = time
    data_dict['omega'] = omega
    data_dict['F_q_t_A_A'] = np.linspace(-1, 1, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    data_dict['F_q_t_B_B'] = np.linspace(-0.4, 0.4, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    data_dict['F_q_t_B_B'] = np.linspace(-0.3, 0.3, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    data_dict['S_q_w_A_A'] = np.linspace(-10, 10, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    data_dict['S_q_w_B_B'] = np.linspace(-4, 4, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    data_dict['S_q_w_B_B'] = np.linspace(-3, 3, N_qpoints * window_size).reshape(
        (N_qpoints, window_size)
    )
    return data_dict


@pytest.fixture
def data_dict_with_self(q_points, time, omega):
    # setup data dict
    data_dict = dict()
    data_dict['q_points'] = q_points
    data_dict['time'] = time
    data_dict['omega'] = omega
    for key, minval, maxval in [
        ('F_q_t_A_A', -1, 1),
        ('F_q_t_B_B', -0.4, 0.4),
        ('F_q_t_A_B', -0.3, 0.3),
        ('F_s_q_t_A', -0.3, 0.3),
        ('F_s_q_t_B', -0.3, 0.3),
        ('S_q_w_A_A', -10, 10),
        ('S_q_w_B_B', -4, 4),
        ('S_q_w_A_B', -3, 3),
        ('S_s_q_w_A', -3, 3),
        ('S_s_q_w_B', -3, 3),
        ('F_q_t_incoh', -1, 1),
        ('F_q_t_coh',   -1, 1),
        ('S_q_w_incoh', -1, 1),
        ('S_q_w_coh',   -1, 1),
    ]:
        data_dict[key] = \
            np.linspace(minval, maxval, N_qpoints * window_size).reshape((N_qpoints, window_size))
    return data_dict


@pytest.fixture
def data_dict_without_self(q_points, time, omega):
    # setup data dict
    data_dict = dict()
    data_dict['q_points'] = q_points
    data_dict['time'] = time
    data_dict['omega'] = omega
    for key, minval, maxval in [
        ('F_q_t_A_A', -1, 1),
        ('F_q_t_B_B', -0.4, 0.4),
        ('F_q_t_A_B', -0.3, 0.3),
        ('S_q_w_A_A', -10, 10),
        ('S_q_w_B_B', -4, 4),
        ('S_q_w_A_B', -3, 3),
        ('F_q_t_coh', -1, 1),
        ('S_q_w_coh', -1, 1),
    ]:
        data_dict[key] = \
            np.linspace(minval, maxval, N_qpoints * window_size).reshape((N_qpoints, window_size))
    return data_dict


@pytest.fixture
def simple_sample(data_dict):
    meta_data = {'type': 'data_dict'}
    sample = Sample(data_dict, **meta_data)
    return sample


@pytest.fixture
def sample_with_self(data_dict_with_self):
    meta_data = {'type': 'data_dict_with_self'}
    sample = Sample(data_dict_with_self, **meta_data)
    return sample


@pytest.fixture
def sample_without_self(data_dict_without_self):
    meta_data = {'type': 'data_dict_without_self'}
    sample = Sample(data_dict_without_self, **meta_data)
    return sample
