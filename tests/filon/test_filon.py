import tempfile
import os
import itertools
import numpy as np
from dynasor.post_processing.filon import _gen_sc_int, _alpha_beta_gamma_single

np.random.seed(42)
ref = '0100010001000010111010111111110011101000001111101101010110000000'
assert ''.join(map(str, np.random.choice([0, 1], 64))) == ref

Nk = [1, 2]
Nx = [5, 7]
magic_numbers = [0, 0.0, 1, 1.0, -1, -1.0, np.pi, 2*np.pi, 100, 2**0.5, 0.42]
dims = [1, 2, 3]


def test_abg():
    thetas = np.array([0.0, 3.14, -1, 0.5])

    # 3x4 due to historical reasons
    res = np.array([[0.00000000, 0.31830964, -0.03850188, 0.00536042],
                    [0.66666667, 0.40590123,  0.76525831, 0.69767347],
                    [1.33333333, 0.40590123,  1.20467472, 1.30029625]])

    new = np.array([_alpha_beta_gamma_single(t) for t in thetas])  # 4x3
    assert np.allclose(res, new.T)


def test_filon_1D():

    iterations = 100

    results = []

    np.random.seed(42)

    for nx, nk, sc in itertools.product(Nx, Nk, [np.sin, np.cos]):
        for i in range(iterations):
            f = np.random.choice(magic_numbers, nx)
            k = np.random.choice(magic_numbers, nk)
            dx = np.random.choice(magic_numbers)
            x0 = np.random.choice(magic_numbers)

            integral = _gen_sc_int(f, dx, k, x0, 0, sc)

            results.extend(integral)

    path = os.path.join(os.path.dirname(__file__), 'results_filon_1D_ref.txt')

    with tempfile.NamedTemporaryFile() as f:
        np.savetxt(f.name, results)
        res = np.loadtxt(f.name)

    res_ref = np.loadtxt(path)

    assert np.allclose(res, res_ref)


def test_filon_ND():

    iterations = 10

    results = []

    np.random.seed(42)

    for nx, nk, sc, dim in itertools.product(Nx, Nk, [np.sin, np.cos], dims):
        for i in range(iterations):
            f = np.random.choice(magic_numbers, (nx,) * dim)
            k = np.random.choice(magic_numbers, nk)
            dx = np.random.choice(magic_numbers)
            x0 = np.random.choice(magic_numbers)
            d = np.random.choice(range(dim))

            integral = _gen_sc_int(f, dx, k, x0, d, sc)

            results.extend(integral.flatten())

    path = os.path.join(os.path.dirname(__file__), 'results_filon_ND_ref.txt')

    with tempfile.NamedTemporaryFile() as f:
        np.savetxt(f.name, results)
        res = np.loadtxt(f.name)

    res_ref = np.loadtxt(path)

    assert np.allclose(res, res_ref)
