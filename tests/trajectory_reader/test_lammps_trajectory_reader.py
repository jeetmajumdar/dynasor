import numpy as np
import os
import pytest
from dynasor.trajectory.lammps_trajectory_reader import LammpsTrajectoryReader


@pytest.fixture
def first_frame():
    data = np.array([
        [1, 1, 1.91468, 3.02071, 0.528818, 0.00105876, 0.000389945, 0.000540445],
        [2, 2, 1.29395, 3.5652, 6.21471, 0.0116399, 0.0238521, 0.0115804],
        [2, 3, 1.39919, 2.29819, 0.989518, -0.0190113, 0.0234721, 0.0142206],
        [1, 4, 3.626, 4.14767, 2.28901, -0.00775121, -0.00332156, 0.00045625],
        [2, 5, 4.16502, 3.38633, 2.64931, -0.00916183, -0.00898758, -0.0093385],
        [2, 6, 3.00361, 3.81053, 1.58263, -0.00415819, 0.00792349, -0.00812868],
        [1, 7, 2.42769, 6.19665, 3.54307, 0.00516741, 0.00576708, -0.00187862],
        [2, 8, 3.06452, 0.52461, 4.05338, 0.0147389, -0.00707458, 0.000731629],
        [2, 9, 2.90869, 5.38709, 3.20654, -0.00964617, -0.00368993, -0.00034467],
        [1, 10, 3.80235, 1.58899, 5.41742, 0.000160311, 0.000400954, -0.00271396],
        [2, 11, 4.36344, 2.15982, 4.81798, 0.0317508, -0.00780526, 0.0190895],
        [2, 12, 3.13529, 2.16145, 5.8942, 0.00501066, 0.0133524, -0.0114753],
        [1, 13, 4.80923, 5.75237, 0.272529, -0.0041924, 0.00248113, 5.02226e-05],
        [2, 14, 4.24226, 0.143661, 6.00552, -0.00864071, 0.00125601, 0.00341038],
        [2, 15, 4.22911, 5.2088, 0.879155, 0.00156727, 0.00697033, 0.00957028],
        [1, 16, 0.417374, 1.05515, 1.90862, 0.00143456, -0.00280331, -0.00114098],
        [2, 17, 6.07448, 0.524278, 1.30311, 0.0218267, -0.00281436, -0.0211203],
        [2, 18, 0.957417, 0.433166, 2.47562, -0.0062517, -0.00294501, 0.00603581],
        [1, 19, 5.4205, 2.90285, 3.66719, 0.00245983, 0.000292166, 0.00455708],
        [2, 20, 5.97449, 2.21136, 3.20358, 0.00989121, 0.00880165, 0.000767886],
        [2, 21, 6.01398, 3.63229, 4.00734, -0.00868655, 0.0196349, -0.0174331],
        [1, 22, 0.616258, 4.77853, 5.1262, -0.000836257, -0.00691637, 0.000166677],
        [2, 23, 1.20745, 5.48874, 4.74398, 0.00678782, -0.0119932, 0.00252133],
        [2, 24, 6.16478, 5.20174, 5.69963, 0.00200832, -0.001067, -0.00067658],
        ])
    return data


@pytest.fixture
def filename_lammpstrj():
    this_dir = os.path.dirname(__file__)
    return os.path.join(this_dir, 'trajectory_files/positions_and_velocities.lammpstrj')


@pytest.fixture
def filename_lammpstrj_no_velocities():
    this_dir = os.path.dirname(__file__)
    return os.path.join(this_dir, 'trajectory_files/positions.lammpstrj')


def test_open_lammpstrj_no_velocities(filename_lammpstrj_no_velocities):
    reader = LammpsTrajectoryReader(filename_lammpstrj_no_velocities)
    reader.close()


def test_open_lammpstrj(filename_lammpstrj):
    reader = LammpsTrajectoryReader(filename_lammpstrj)
    reader.close()


def test_read_frames(filename_lammpstrj):
    reader = LammpsTrajectoryReader(filename_lammpstrj)
    frames = list(reader)
    assert len(frames) == 4
    reader.close()


def test_first_frame_contents(filename_lammpstrj, first_frame):
    positions_target = first_frame[:, 2:5]
    velocities_target = first_frame[:, 5:]
    reader = LammpsTrajectoryReader(filename_lammpstrj)
    frame = next(reader)
    assert frame.n_atoms == 24
    assert np.allclose(frame.positions, positions_target)
    assert np.allclose(frame.velocities, velocities_target)
    reader.close()


def test_first_frame_contents_no_velocities(filename_lammpstrj_no_velocities, first_frame):
    positions_target = first_frame[:, 2:5]
    reader = LammpsTrajectoryReader(filename_lammpstrj_no_velocities)
    frame = next(reader)
    assert frame.n_atoms == 24
    assert frame.velocities is None
    assert np.allclose(frame.positions, positions_target)
    reader.close()


def test_first_frame_contents_with_units(filename_lammpstrj, first_frame):
    positions_target = first_frame[:, 2:5]
    velocities_target = first_frame[:, 5:]
    reader = LammpsTrajectoryReader(filename_lammpstrj, length_unit='nm', time_unit='ps')
    frame = next(reader)
    assert frame.n_atoms == 24
    assert np.allclose(frame.positions, 10 * positions_target)
    assert np.allclose(frame.velocities, 1 / 100 * velocities_target)

    # cell
    cell_target = 10 * 6.25 * np.eye(3)
    assert np.allclose(frame.cell, cell_target)
