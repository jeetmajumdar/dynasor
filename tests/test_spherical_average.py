import numpy as np
from dynasor.post_processing.spherical_average import _compute_spherical_average


def test_spherical_averaging():
    N_q = 100
    N_t = 8
    qbins = 20

    np.random.seed(42)
    data = np.random.random((N_q, N_t))
    q_points = np.random.random((N_q, 3))

    q_bincenters, bin_counts, averaged_data = _compute_spherical_average(q_points, data, qbins)
    assert len(q_bincenters) == qbins
    assert averaged_data.shape == (qbins, N_t)

    # check q_bincenters
    q_min = np.min(np.linalg.norm(q_points, axis=1))
    q_max = np.max(np.linalg.norm(q_points, axis=1))
    assert np.isclose(q_bincenters[0], q_min)
    assert np.isclose(q_bincenters[-1], q_max)

    # check bin counts
    bin_counts_target = np.array([2, 2, 3, 1, 4, 0, 5, 10, 9, 16, 7, 11, 5, 6, 8, 4, 5, 0, 1, 1])
    assert np.allclose(bin_counts, bin_counts_target)

    # check for Nans for empty bins
    for bin_index in np.where(bin_counts == 0)[0]:
        assert np.all(np.isnan(averaged_data[bin_index]))

    # check averaged data
    target_average = np.array([0.59242063, 0.44607297, 0.45898736, 0.64679424, 0.66729803,
                               0.45030113, 0.417301, 0.59437702])
    assert np.allclose(averaged_data[10], target_average)
