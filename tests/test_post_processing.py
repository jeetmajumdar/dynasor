from typing import Dict

import numpy as np
import pytest

from dynasor.correlation_functions import compute_static_structure_factors
from dynasor.post_processing import neutron_cross_sections
from dynasor.sample import Sample
from dynasor.trajectory import Trajectory


def _compute_expected(
    initial: Sample,
    weighted: Sample,
    pattern: str,
    coherent_scattering_lengths: Dict,
    incoherent_scattering_lengths: Dict = None,
):
    expected_coherent = np.zeros(initial['S_q_w_A_A'].shape)
    expected_incoherent = np.zeros(initial['S_q_w_A_A'].shape)
    expected_total = np.zeros(initial['S_q_w_A_A'].shape)

    for key in initial.available_correlation_functions:
        if 'coh' in key:
            continue
        if pattern not in key:
            continue
        key_split = key.split('_')
        is_self = key_split[1] == 's'

        if is_self:
            assert incoherent_scattering_lengths is not None,  'must be provided!'
            species = key_split[-1] * 2
            scattering_lengths = incoherent_scattering_lengths
        else:
            scattering_lengths = coherent_scattering_lengths
            species = key_split[-2:]
        expected = (
            initial[key]
            * scattering_lengths[species[0]]
            * scattering_lengths[species[1]]
        )
        computed = weighted[key]
        assert np.allclose(expected, computed, atol=1e-12, rtol=1e-6)

        expected_total += expected
        if is_self:
            expected_incoherent += expected
        else:
            expected_coherent += expected

    return expected_coherent, expected_incoherent, expected_total


def test_neutron_cross_sections_noop(sample_with_self):
    """Set all scattering lengths to 1, should get back initial structure factors"""
    initial = sample_with_self
    coherent_scattering_lengths = {'A': 1.0, 'B': 1.0}
    incoherent_scattering_lengths = {'A': 1.0, 'B': 1.0}
    weighted = neutron_cross_sections(
        sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
    )

    expected_S = np.zeros(initial.S_q_w_A_A.shape)
    expected_F = np.zeros(initial.F_q_t_A_A.shape)
    for key in initial.available_correlation_functions:
        if 'coh' in key:
            continue
        if key in sample_with_self._meta_data_keys:
            continue
        expected = getattr(initial, key)
        computed = getattr(weighted, key)
        assert np.allclose(expected, computed, atol=1e-12, rtol=1e-6)
        if key.startswith('F'):
            expected_F += expected
        else:
            expected_S += expected

    computed_S = weighted.S_q_w
    computed_F = weighted.F_q_t

    assert np.allclose(expected_F, computed_F, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_S, computed_S, atol=1e-12, rtol=1e-6)
    assert sample_with_self.symbols == weighted.symbols
    assert sample_with_self.pair_types == weighted.pair_types
    assert sample_with_self.particle_counts == weighted.particle_counts
    assert sample_with_self.cell == weighted.cell
    assert sample_with_self._meta_data_keys == weighted._meta_data_keys
    for key in list(
            set(sample_with_self._initial_data_keys) -
            set(sample_with_self.available_correlation_functions)):
        # Check that initial keys (such as q_points, time and omega) are copied.
        assert np.allclose(sample_with_self[key], weighted[key], atol=1e-12, rtol=1e-6)


def test_neutron_cross_sections_zero(sample_with_self):
    """Set all scattering lengths to 0, should get back zero."""
    initial = sample_with_self
    coherent_scattering_lengths = {'A': 0.0, 'B': 0.0}
    incoherent_scattering_lengths = {'A': 0.0, 'B': 0.0}
    weighted = neutron_cross_sections(
        sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
    )

    expected_total = np.zeros(initial.S_q_w_A_A.shape)
    computed_total = weighted.S_q_w
    assert np.allclose(expected_total, computed_total, atol=1e-12, rtol=1e-6)
    assert sample_with_self._symbols == weighted._symbols
    assert sample_with_self._pair_types == weighted._pair_types
    assert sample_with_self._particle_counts == weighted._particle_counts
    assert sample_with_self._cell == weighted._cell
    assert sample_with_self._meta_data_keys == weighted._meta_data_keys


def test_neutron_cross_sections_realistic(sample_with_self):
    """Use realistic scattering lengths, and make sure the sum matches the expected value"""
    initial = sample_with_self
    coherent_scattering_lengths = {'A': 70.0, 'B': 2.5}
    incoherent_scattering_lengths = {'A': 5.0, 'B': 10.0}
    weighted = neutron_cross_sections(
        sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
    )

    expected_coh_S, expected_incoh_S, expected_S = _compute_expected(
        initial,
        weighted,
        'S_',
        coherent_scattering_lengths,
        incoherent_scattering_lengths,
    )
    expected_coh_F, expected_incoh_F, expected_F = _compute_expected(
        initial,
        weighted,
        'F_',
        coherent_scattering_lengths,
        incoherent_scattering_lengths,
    )
    computed_S = weighted.S_q_w
    computed_coh_S = weighted.S_q_w_coh
    computed_incoh_S = weighted.S_q_w_incoh

    computed_F = weighted.F_q_t
    computed_coh_F = weighted.F_q_t_coh
    computed_incoh_F = weighted.F_q_t_incoh

    assert np.allclose(expected_S, computed_S, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_coh_S, computed_coh_S, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_incoh_S, computed_incoh_S, atol=1e-12, rtol=1e-6)

    assert np.allclose(expected_F, computed_F, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_coh_F, computed_coh_F, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_incoh_F, computed_incoh_F, atol=1e-12, rtol=1e-6)
    print(sample_with_self._meta_data_keys, weighted._meta_data_keys)
    assert sample_with_self._symbols == weighted._symbols
    assert sample_with_self._pair_types == weighted._pair_types
    assert sample_with_self._particle_counts == weighted._particle_counts
    assert sample_with_self._cell == weighted._cell
    assert sample_with_self._meta_data_keys == weighted._meta_data_keys


def test_neutron_cross_sections_realistic_numeric():
    """Use realistic scattering lengths, and make sure the sum matches the expected numeric value"""
    a, b, c = 1, 0.25, 2.3
    sample = Sample(
        {
            'q_points': np.arange(5),
            'S_q_w_A_A': a * np.ones((5, 10)),
            'S_q_w_A_B': b * np.ones((5, 10)),
            'S_q_w_B_B': c * np.ones((5, 10)),
        }
    )
    initial = sample

    scattering_lengths = {'A': 20.0, 'B': 2.5}
    weighted = neutron_cross_sections(sample, scattering_lengths)

    expected_coh_S, expected_incoh_S, expected_S = _compute_expected(
        initial,
        weighted,
        'S_',
        scattering_lengths,
    )
    expected_coh_F, expected_incoh_F, expected_F = _compute_expected(
        initial,
        weighted,
        'F_',
        scattering_lengths,
    )
    computed_S = weighted['S_q_w']
    computed_coh_S = weighted['S_q_w_coh']

    assert np.allclose(expected_S, computed_S, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_coh_S, computed_coh_S, atol=1e-12, rtol=1e-6)
    assert sample._symbols == weighted._symbols
    assert sample._pair_types == weighted._pair_types
    assert sample._particle_counts == weighted._particle_counts
    assert sample._cell == weighted._cell
    assert sample._meta_data_keys == weighted._meta_data_keys


def test_neutron_cross_sections_without_self(sample_without_self):
    """
    Use realistic scattering lengths, but for a case where self interactions
    have not been calculated. This corresponds to only the coherent dynamic
    structure factor.
    """
    initial = sample_without_self
    coherent_scattering_lengths = {'A': 70.0, 'B': 2.5}
    with pytest.warns(UserWarning) as w:
        weighted = neutron_cross_sections(
            sample_without_self, coherent_scattering_lengths
        )
    assert len(w) == 1
    assert (
        str(w[0].message)
        == ('No self-correlations found. Weighted structure'
            ' factors will only contain the coherent part.')
    )

    expected_coh_S, _, expected_S = _compute_expected(
        initial,
        weighted,
        'S_',
        coherent_scattering_lengths,
    )
    expected_coh_F, _, expected_F = _compute_expected(
        initial,
        weighted,
        'F_',
        coherent_scattering_lengths,
    )
    computed_S = weighted['S_q_w']
    computed_coh_S = weighted['S_q_w_coh']

    computed_F = weighted['F_q_t']
    computed_coh_F = weighted['F_q_t_coh']

    assert np.allclose(expected_S, computed_S, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_coh_S, computed_coh_S, atol=1e-12, rtol=1e-6)

    assert np.allclose(expected_F, computed_F, atol=1e-12, rtol=1e-6)
    assert np.allclose(expected_coh_F, computed_coh_F, atol=1e-12, rtol=1e-6)
    assert sample_without_self._symbols == weighted._symbols
    assert sample_without_self._pair_types == weighted._pair_types
    assert sample_without_self._particle_counts == weighted._particle_counts
    assert sample_without_self._cell == weighted._cell
    assert sample_without_self._meta_data_keys == weighted._meta_data_keys


def test_neutron_cross_sections_invalid_species(sample_with_self):
    """Try to supply an invalid number of atom species in scattering_lengths"""
    with pytest.raises(ValueError) as e:
        coherent_scattering_lengths = {'A': 1.0, 'B': 1.0}
        incoherent_scattering_lengths = {'A': 1.0, 'B': 1.0, 'C': 1.0}
        neutron_cross_sections(
            sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
        )
    assert 'Species for coherent and incoherent does not match' in str(e)

    with pytest.raises(ValueError) as e:
        coherent_scattering_lengths = {'A': 1.0, 'B': 1.0, 'C': 1.0}
        incoherent_scattering_lengths = {'A': 1.0, 'B': 1.0, 'C': 1.0}
        neutron_cross_sections(
            sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
        )
    assert (
        'Number of species in coherent_scattering_lengths does not match sample'
        in str(e)
    )


def test_neutron_cross_sections_wrong_species(sample_with_self):
    """Try to supply an invalid species in scattering_lengths"""
    with pytest.raises(ValueError) as e:
        coherent_scattering_lengths = {'A': 1.0, 'C': 1.0}
        incoherent_scattering_lengths = {'A': 1.0, 'C': 1.0}
        neutron_cross_sections(
            sample_with_self, coherent_scattering_lengths, incoherent_scattering_lengths
        )
    assert "Species {'C'} not found in sample" in str(e)


def test_neutron_cross_sections_missing_incoherent(sample_with_self):
    """Try to run with incoherent scattering lengths missing"""
    with pytest.raises(ValueError) as e:
        scattering_lengths = {'A': 1.0, 'B': 1.0}
        neutron_cross_sections(sample_with_self, scattering_lengths)
    assert 'incoherent_scattering_lengths is required when self-part is present' in str(
        e
    )


def test_neutron_cross_sections_static_structure_factor(traj_fname_xyz_long, q_points):
    """Computes static structure factors and checks cross section weighting"""
    # setup
    n_atoms = 320
    n_A = 50
    atomic_indices = dict()
    atomic_indices['A'] = np.arange(0, n_A, 1)
    atomic_indices['B'] = np.arange(n_A, n_atoms, 1)
    coherent_scattering_lengths = {'A': 70.0, 'B': 2.5}
    incoherent_scattering_lengths = {'A': 5.0, 'B': 10.0}

    # calculate static
    traj = Trajectory(
        traj_fname_xyz_long, trajectory_format='extxyz', atomic_indices=atomic_indices)
    result = compute_static_structure_factors(traj, q_points=q_points)

    # Weight result
    weighted = neutron_cross_sections(
        result, coherent_scattering_lengths, incoherent_scattering_lengths
    )
    for key in result.available_correlation_functions:
        assert key in weighted.available_correlation_functions
    assert 'S_q_w' in weighted.available_correlation_functions
    assert 'S_q_w_coh' in weighted.available_correlation_functions
