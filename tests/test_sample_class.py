from tempfile import NamedTemporaryFile

import numpy as np
import pytest

from dynasor.sample import Sample, read_sample_from_npz

N_qpoints = 20
window_size = 100


@pytest.fixture
def q_points():
    return np.array([np.array([1, 0, 0]) * amp for amp in np.linspace(0, 1, N_qpoints)])


@pytest.fixture
def time():
    return np.linspace(0, 10, window_size)


@pytest.fixture
def omega():
    return np.linspace(0, 5, window_size)


@pytest.fixture
def data_dict(q_points, time, omega):
    # setup data dict
    data_dict = dict()
    data_dict['q_points'] = q_points
    data_dict['time'] = time
    data_dict['omega'] = omega
    data_dict['F_q_t_A_A'] = np.linspace(-1, 1, N_qpoints * window_size).reshape((N_qpoints,
                                                                                  window_size))
    data_dict['F_q_t_B_B'] = np.linspace(-0.4, 0.4, N_qpoints * window_size).reshape((N_qpoints,
                                                                                      window_size))
    data_dict['F_q_t_A_B'] = np.linspace(-0.3, 0.3, N_qpoints * window_size).reshape((N_qpoints,
                                                                                      window_size))
    data_dict['S_q_w_A_A'] = np.linspace(-10, 10, N_qpoints * window_size).reshape((N_qpoints,
                                                                                    window_size))
    data_dict['S_q_w_B_B'] = np.linspace(-4, 4, N_qpoints * window_size).reshape((N_qpoints,
                                                                                  window_size))
    data_dict['S_q_w_A_B'] = np.linspace(-3, 3, N_qpoints * window_size).reshape((N_qpoints,
                                                                                  window_size))
    return data_dict


@pytest.fixture
def meta_data():
    symbols = ['A', 'B']
    pair_types = [('A', 'A'), ('A', 'B'), ('B', 'B')]
    counts = dict(A=500, B=250)
    cell = np.diag([2.5, 3.8, 2.95])
    meta_data = dict(symbols=symbols, pair_types=pair_types, particle_counts=counts, cell=cell)
    return meta_data


@pytest.fixture
def simple_sample(data_dict, meta_data):
    sample = Sample(data_dict, symbols=meta_data['symbols'], pair_types=meta_data['pair_types'],
                    particle_counts=meta_data['particle_counts'], cell=meta_data['cell'])
    return sample


def test_getattributes(q_points, time, simple_sample):
    assert np.allclose(simple_sample.q_points, q_points)
    assert np.allclose(simple_sample.time, time)


def test_getitem(data_dict, simple_sample):
    for key in data_dict:
        assert np.allclose(data_dict[key], getattr(simple_sample, key))

    # check that sample.xyz and sample['xyz'] return the same thing for an item in data_dict and
    # one in meta_data
    assert np.allclose(simple_sample.cell, simple_sample['cell'])
    assert np.allclose(simple_sample.S_q_w_B_B, simple_sample['S_q_w_B_B'])


def test_get_property_none(data_dict, meta_data):
    """Should return None for properties which are not defined"""
    sample_with_property = Sample(data_dict, symbols=meta_data['symbols'], pair_types=None)
    sample_without_properties = Sample(data_dict)

    assert sample_with_property.symbols == meta_data['symbols']
    assert sample_with_property.pair_types is None
    assert sample_without_properties.symbols is None
    assert sample_without_properties.pair_types is None
    assert sample_without_properties.particle_counts is None
    assert sample_without_properties.cell is None


def test_repr_str(simple_sample):
    s1 = repr(simple_sample)
    s2 = str(simple_sample)
    assert isinstance(s1, str)
    assert s1 == s2


def test_read_and_write(data_dict, meta_data, simple_sample):

    # write to file
    tempfile = NamedTemporaryFile(suffix='.npz')
    simple_sample.write_to_npz(tempfile.name)
    for key, val in data_dict.items():
        assert np.allclose(data_dict[key], getattr(simple_sample, key))

    # read from file
    new_sample = read_sample_from_npz(tempfile.name)

    # check that nothing changed with metadata
    assert meta_data['symbols'] == new_sample.symbols
    assert meta_data['pair_types'] == new_sample.pair_types
    assert np.allclose(meta_data['cell'], new_sample.cell)
    for key in meta_data['particle_counts']:
        assert meta_data['particle_counts'][key] == new_sample.particle_counts[key]

    # check that nothing changed with correlation function data
    for key, val in data_dict.items():
        assert np.allclose(data_dict[key], getattr(new_sample, key))
