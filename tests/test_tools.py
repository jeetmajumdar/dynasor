import numpy as np
from dynasor.tools.acfs import compute_acf, fermi_dirac, smoothing_function
import pytest


@pytest.fixture
def signal_complex():
    np.random.seed(42)
    N = 1000
    x = np.random.random((N, ))
    y = np.random.random((N, ))
    Z_t = x + 1j * y
    return Z_t


def test_numpy_scipy_acf(signal_complex):
    delta_t = 0.25
    t1, acf1 = compute_acf(signal_complex, delta_t=delta_t, method='numpy')
    t2, acf2 = compute_acf(signal_complex, delta_t=delta_t, method='scipy')
    assert np.allclose(t1, t2)
    assert np.allclose(acf1, acf2)


def test_compute_acf_invalid_arguments(signal_complex):
    with pytest.raises(ValueError):
        t1, acf1 = compute_acf(signal_complex, method='asd')


def test_fermi_dirac_time_function(signal_complex):
    signal = signal_complex.real
    time = np.arange(0, 1000, 1)
    t_0 = 500
    t_width = 20
    f = fermi_dirac(time, t_0, t_width)
    signal_damped = f * signal
    assert np.isclose(signal_damped[0], signal[0])
    assert np.isclose(signal_damped[-1], 0.0)


def test_smoothing_function():
    data = np.array([1, 2, 2.5, 4, 5.5, 6.5, 7, 8, 7, 2])

    res1 = smoothing_function(data, window_size=1, window_type='boxcar')
    assert np.allclose(res1, data)

    res2 = smoothing_function(data, window_size=2, window_type='boxcar')
    assert len(res2) == len(data)
    assert np.allclose(res2[0], 1)
    assert np.allclose(res2[1], 1.5)
    assert np.allclose(res2[2], 2.25)

    res3 = smoothing_function(data, window_size=3, window_type='boxcar')
    assert len(res3) == len(data)
    assert np.allclose(res3[0], 1.5)
    assert np.allclose(res3[1], 5.5/3)
    assert np.allclose(res3[2], 8.5/3)
    assert np.allclose(res3[-1], 4.5)
