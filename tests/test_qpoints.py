import numpy as np
from dynasor.qpoints import generate_spherical_qpoints


def test_generate_spherical_qpoints():

    cell = np.diag([2.2, 3.76, 4.01])
    q_max = 20

    # without pruning
    q_points = generate_spherical_qpoints(cell, q_max)
    assert q_points.shape[0] == 693
    assert q_points.shape[1] == 3
    assert np.max(np.linalg.norm(q_points, axis=1)) <= q_max

    # with pruning
    max_points = 250
    q_points = generate_spherical_qpoints(cell, q_max, max_points=max_points)

    assert q_points.shape[0] < max_points + 100
    assert q_points.shape[1] == 3
    assert np.max(np.linalg.norm(q_points, axis=1)) <= q_max


def test_generate_spherical_qpoints_with_seed():
    cell = np.diag([2.2, 3.76, 4.01])
    q_max = 30
    max_points = 1500
    q_points1 = generate_spherical_qpoints(cell, q_max, max_points=max_points, seed=42)
    q_points2 = generate_spherical_qpoints(cell, q_max, max_points=max_points, seed=43)
    q_points3 = generate_spherical_qpoints(cell, q_max, max_points=max_points, seed=42)
    assert np.allclose(q_points1, q_points3)
    assert q_points1.shape != q_points2.shape
