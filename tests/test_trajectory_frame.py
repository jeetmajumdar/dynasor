import pytest
import numpy as np
from dynasor.trajectory.trajectory_frame import TrajectoryFrame
from dynasor.trajectory import Trajectory


def test_get_as_arrays():

    # setup
    atomic_indices_ref = dict()
    atomic_indices_ref['Cs'] = np.array([0, 1, 2, 3, 4, 9])
    atomic_indices_ref['Pb'] = np.array([5, 6, 7, 8])
    positions_ref = np.arange(0, 30, 1).reshape((10, 3))
    velocities_ref = np.arange(-15, 15, 1).reshape((10, 3))
    frame = TrajectoryFrame(atomic_indices_ref, 0, positions_ref, velocities_ref)

    # get arrays with complete atomic_indices
    assert np.allclose(frame.get_positions_as_array(atomic_indices_ref), positions_ref)
    assert np.allclose(frame.get_velocities_as_array(atomic_indices_ref), velocities_ref)

    # ValueError with incomplete atomic_indices, len(all_inds) != n_atoms
    atomic_indices = dict()
    atomic_indices['Cs'] = [0, 1, 2, 3, 4]
    atomic_indices['Pb'] = [5, 6, 7, 8]

    with pytest.raises(ValueError):
        frame.get_positions_as_array(atomic_indices)
    with pytest.raises(ValueError):
        frame.get_velocities_as_array(atomic_indices)

    # ValueError with incomplete atomic_indices, len(set(all_inds)) != n_atoms
    atomic_indices = dict()
    atomic_indices['Cs'] = [0, 1, 2, 3, 4, 4]
    atomic_indices['Pb'] = [5, 6, 7, 8]

    with pytest.raises(ValueError):
        frame.get_positions_as_array(atomic_indices)
    with pytest.raises(ValueError):
        frame.get_velocities_as_array(atomic_indices)


def test_str():
    atomic_indices = dict()
    atomic_indices['Cs'] = np.arange(0, 20)
    atomic_indices['Pb'] = np.arange(50, 100)

    # No velocities
    traj = Trajectory('tests/trajectory_reader/trajectory_files/dump.xyz',
                      trajectory_format='extxyz', atomic_indices=atomic_indices)
    str_target = 'Frame index 0\n  positions Cs shape (20, 3)\n  positions Pb shape (50, 3)'
    frame = next(traj)
    assert str(frame) == str_target

    # With velocities
    traj = Trajectory('tests/trajectory_reader/trajectory_files/dump_long_with_velocities.xyz',
                      trajectory_format='extxyz', atomic_indices=atomic_indices)

    str_target = 'Frame index 0\n  positions Cs shape (20, 3)\n  positions Pb shape (50, 3)\n'\
                 '  velocities Cs shape (20, 3)\n  velocities Pb shape (50, 3)'
    frame = next(traj)
    assert str(frame) == str_target
