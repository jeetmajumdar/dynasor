Tutorials
*********

A collection of short tutorials.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 1

   time_convergence
   acfs_and_ffts