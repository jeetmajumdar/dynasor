.. _theory:

.. index:: Theory
.. index:: Background

Theoretical background
**********************

Below follows a brief overview of the theory underpinning the correlation functions supported by :program:`dynasor` as well as their analysis.
For a more detailed description and derivations of the correlations functions see for example :cite:`BoonYip`.

.. index:: Correlation functions; Position


Dynamic structure factor
------------------------

The particle density is denoted :math:`n(\boldsymbol{r},t)` and defined as

.. math::

   n(\boldsymbol{r},t) = \sum _i ^N \delta (\boldsymbol{r} - \boldsymbol{r}_i(t)),

where :math:`N` is the number of particles and :math:`\boldsymbol{r}_i(t)` is the position of particle :math:`i` at time :math:`t`.
The intermediate scattering function :math:`F(\boldsymbol{q},t)` is defined as the Fourier transform of the auto-correlation of the particle density

.. math::

   F(\boldsymbol{q},t)=\frac{1}{N}\left<n(\boldsymbol{q},t)n(-\boldsymbol{q},0)\right>
   \,= \, \frac{1}{N}\sum _i ^N \sum _j ^N \left<
   \mathrm{exp}
   \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right]
   \right>,

where :math:`\boldsymbol{q}` is a wave vector.
The brackets denote an ensemble average, which in the case of :program:`dynasor` is replaced by a time average.
(The system under study should be ergodic in order for the time average to be a suitable substitute for the ensemble average.)

.. index:: Self part
	   
The self-part of the intermediate scattering function (the incoherent part), i.e., :math:`i=j`, is defined as

.. math::

   F_s(\boldsymbol{q},t)=\frac{1}{N} \sum _i ^N \left <
   \mathrm{exp}\left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_i(0))\right]
   \right >

.. index:: Incoherent scattering function
.. index:: Scattering function; Incoherent

and describes single particle motion, i.e., diffusive motion.
:math:`F_s(\boldsymbol{q},t)` is commonly referred to as the incoherent scattering function.

The static structure factor is given by

.. math::

   S(\boldsymbol{q}) = F(\boldsymbol{q},0),

.. index:: Structure factor; dynamic
.. index:: Dynamic structure factor
		     
whereas the dynamic structure factor :math:`S(\boldsymbol{q},\omega)` is the Fourier transform of :math:`F(\boldsymbol{q},t)`

.. math::

   S(\boldsymbol{q},\omega) = \int _{-\infty} ^\infty
   F(\boldsymbol{q},t) \, e^{-iwt} \mathrm{d}t,

where :math:`\omega` is the angular frequency.
:math:`S(\boldsymbol{q},\omega)` exhibits peaks in the :math:`(\boldsymbol{q},\omega)` plane corresponding to the dispersion of lattice vibrations (phonons).
The widths of these peaks are related to the phonon lifetimes.
Computing :math:`S(\boldsymbol{q},\omega)` via molecular dynamics simulation has the advantage of *fully* including anharmonic effects and allowing one to study the temperature dependence of phonon dispersions.


.. index:: Correlation functions; Velocity
.. index:: Correlation functions; Current
.. index:: Velocity correlation functions
.. index:: Current correlation functions

Velocity correlation functions
------------------------------

It is often convenient to also consider current correlations based on the particle velocities.
The current density :math:`\boldsymbol{j}(\boldsymbol{r},t)` is given by

.. math::

   \boldsymbol{j}(\boldsymbol{r},t)
   &= \sum_i^N \boldsymbol{v}_i(t) \, \delta (\boldsymbol{r} - \boldsymbol{r}_i(t)) \\
   \boldsymbol{j}(\boldsymbol{q},t)
   &= \sum_i^N \boldsymbol{v}_i(t) \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)},

where :math:`\boldsymbol{v}_i(t)` is the velocity of particle :math:`i` at time :math:`t`.
This can be split into a longitudinal and transverse part as

.. math::

   \boldsymbol{j}_L(\boldsymbol{q},t)
   &= \sum_i ^N(\boldsymbol{v_i}(t) \cdot\hat{\boldsymbol{q}}) \, \hat{\boldsymbol{q}}
   \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)} \\
   \boldsymbol{j}_T(\boldsymbol{q},t)
   &= \sum_i ^N \left[\boldsymbol{v_i}(t) - (\boldsymbol{v_i}(t) \cdot \hat{\boldsymbol{q}}) \, \hat{\boldsymbol{q}}\right]
   \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)}.

Now the correlation functions can be computed as

.. math::

   C_L(\boldsymbol{q},t) &= \frac{1}{N}\left<\boldsymbol{j}_L(\boldsymbol{q},t)\cdot\boldsymbol{j}_L(-\boldsymbol{q},0)\right> \\
   C_T(\boldsymbol{q},t) &= \frac{1}{N}\left<\boldsymbol{j}_T(\boldsymbol{q},t)\cdot\boldsymbol{j}_T(-\boldsymbol{q},0)\right>.

The longitudinal current can be related to the particle density as

.. math::

   \frac{\partial }{\partial t}n(\boldsymbol{q}, t)
   &= \mathrm{i} \boldsymbol{q}\cdot \boldsymbol{j}(\boldsymbol{q}, t) \\
   \omega^2 S(\boldsymbol{q},\omega)
   &= q^2 C_L(\boldsymbol{q},\omega),

which means some features can be easier to resolve in one function than the other.
The current correlation functions can be thought of as spatially-dependent generalization of the velocity correlation function and are closely related to the phonon spectral energy density.


Multi component systems
-----------------------

In multi-component systems one can introduce partial correlations functions, which enables separation of the contributions by groups of particles, e.g., by type or site symmetry.
For example, in the case of a binary system, one can define :math:`F_{AA}(\boldsymbol{q},t)`, :math:`F_{BB}(\boldsymbol{q},t)`, :math:`F_{AB}(\boldsymbol{q},t)` and so on for all correlation functions introduced above.
In :program:`dynasor` we define the partial correlaion functions as

.. math::

   F_{AA}(\boldsymbol{q},t) = \frac{1}{N}\sum_{i\in A}^{N_A} \sum_{j\in A}^{N_A} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>

where the sums runs over all A atoms.
Here, :math:`N_A` refers to number of atoms of type A, :math:`N_B` refers to number of atoms of type B, and :math:`N = N_A + N_B` refers to the total number of atoms in the system.
For the cross-term, :math:`F_{AB}(\boldsymbol{q},t)`, we get

.. math::

   F_{AB}(\boldsymbol{q},t) = & \frac{1}{N}\sum_{i\in A}^{N_A} \sum_{j\in B}^{N_B} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>
   +
   \frac{1}{N}\sum_{i\in B}^{N_B} \sum_{j\in A}^{N_A} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right> \\=& \frac{2}{N}\sum_{i\in A}^{N_A} \sum_{j\in B}^{N_B} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>

Here, the factor two in the final expression is due to considering both the A-B and B-A terms.
These normalization choices means the total intermediate scattering function :math:`F(\boldsymbol{q},t)` (as defined above) is given by

.. math::
   
   F(\boldsymbol{q},t) = F_{AA}(\boldsymbol{q},t) + F_{AB}(\boldsymbol{q},t) + F_{BB}(\boldsymbol{q},t)

In some cases, instead of analyzing the partial functions directly, it is interesting to consider linear combinations of them. For example weighting them with relevant atomic form factors, masses or charges etc.

Relation to scattering experiments
----------------------------------

The correlation functions can be convoluted with atomic form factors and cross sections in order to obtain predictions for various types of neutron and X-ray scattering experiments.

In solids, it is frequently desirable to determine the above mentioned quantites along specific paths between high-symmetry :math:`\boldsymbol{q}`-points.
In isotropic samples, such as for example liquids, it is on the other hand usually preferable to compute these functions with respect to :math:`q=|\boldsymbol{q}|`, i.e., a spherical average over wave vectors.


Damped harmonic oscillator model
--------------------------------

.. index:: Harmonic oscillator; Damped
.. index:: Damped harmonic Oscillator

The correlation functions can be fitted to analytical expressions for damped harmonic oscillators in order to extract phonon frequencies and phonon lifetimes.
While many different analytical forms can be considered for the damped harmonic oscillator, here we use the assumption that the correlation function of the position of the damped harmonic oscillator has a zero derivative at :math:`t=0`.
The position correlation function (corresponding to the intermediate scattering function) of a damped harmonic oscillator is given by

.. math::

   F(t) &= A \mathrm{e}^{-\Gamma t/2} \big(\cos{ \omega_e t} + \frac{\Gamma}{2\omega_e}\sin{ \omega_e t}\big), \quad \omega_0 > \frac{\Gamma}{2} \\ 
   F(t) &= A \mathrm{e}^{-\Gamma t/2} \big(\cosh{|\omega_e| t} + \frac{\Gamma}{2|\omega_e|}\sinh{|\omega_e| t}\big), \quad \omega_0 < \frac{\Gamma}{2}


where the fitting parameters are :math:`\omega_0`, :math:`\Gamma` and :math:`A`.
The first case, :math:`\omega_0 > \Gamma/2` corresponds to an underdamped oscillator, and the second one to an overdamped oscillator.
The eigenfrequency of the damped oscillator is :math:`\omega_e = \sqrt{\omega_0^2 - \Gamma^2/4}`.
The fitting can also be carried out in the frequency domain by fitting the dynamic structure factor to the following analytical function

.. math::

   a(\omega) = A\frac{2\Gamma \omega_0^2}{(\omega^2 - \omega_0^2)^2 + (\Gamma\omega)^2}.

The intermediate scattering function and dynamic structure factor can thus be analyzed by fitting them to the expressions above in order to extract frequency :math:`\omega_0` and damping :math:`\Gamma`.


This analysis can be extended to the velocity correlation for the damped harmonic oscillator (corresponding to the current correlations) by considering the relation between the longitudinal current correlation and the dynamic structure factor, which gives the following solutions

.. math::

   b(\omega) = B\frac{2\Gamma \omega^2}{(\omega^2 - \omega_0^2)^2 + (\Gamma\omega)^2}.

In the time domain this function becomes

.. math::

   b(t) &= B \mathrm{e}^{-\Gamma t/2} \big(\cos{ \omega_e t} - \frac{\Gamma}{2\omega_e}\sin{ \omega_e t} \big), \quad \omega_0 > \frac{\Gamma}{2} \\ 
   b(t) &= B \mathrm{e}^{-\Gamma t/2} \big(\cosh{|\omega_e| t} - \frac{\Gamma}{2|\omega_e|}\sinh{|\omega_e| t} \big), \quad \omega_0 < \frac{\Gamma}{2}


.. index:: Phonon life time
.. index:: Life times; phonon

Lastly, the damping :math:`\Gamma` is related to the inverse phonon lifetime :math:`\tau` according to

.. math:: \tau = \frac{2}{\Gamma}

or in energy units as

.. math::  \tau = \frac{2\hbar}{\Gamma},

where :math:`\hbar` appears since :math:`\Gamma` has units of rad/s.

If multiple modes are present in a correlation function they should be modeled as a sum of damped harmonic oscillators.


Spectral energy density (SED) method
------------------------------------
.. index:: Spectral energy density (SED)

The spectral energy density is closely related to the phonon dispersion and thus also to the current correlations above.
It is a measure of how the kinetic energy is partitioned over different wavelengths and frequencies in the system.
The theoretical background for SED is quite simple and can be derived in several ways.
The total, integrated kinetic energy of the infinite system can formally be written as

.. math::
        
        E = \int_t \sum_n \sum_i \frac{1}{2} m_i \|v_i(n, t)\|^2.

Here :math:`n` is the cell 3D-index, :math:`i` is the basis index which together uniquely identifies each atom in an infinite crystal.
By using Parsevals theorem for both the time integration and space summation the above is transformed to inverse spatial :math:`q`-space and inverse temporal :math:`\omega`-space

.. math::

        E = \int_w \sum_q \sum_i \frac{1}{2} m_i \|v_i(q, w)\|^2.


This is motivated by the Wiener–Khinchin theorem and the SED is simply defined as the quantity

.. math::

        \text{SED}(q, w) = \sum_i \frac{1}{2} m_i \|v_i(q, w)\|^2

For some more in-depth discussion see e.g. :cite:`Thomas2010`.
