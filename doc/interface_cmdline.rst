.. _interface_cmdline:

.. index:: Interface; command line

Command line interface
**********************

:program:`dynasor` can be run via the command line and expects a number of input parameters.
To show all available options run::

   dynasor --help


Trajectory options
-------------------
You need to specify the format of your trajectory via the `--trajectory-format` flag, for example ::

    dynasor dump.xyz --trajectory-format=extxyz

would use the internal xyz reader (for more info see the :ref:`workflow page
<work_flow>`).


q-point sampling options
------------------------

For example, to sample along a specific path in :math:`\boldsymbol{q}`-space, one can run ::

   dynasor dump.xyz --trajectory-format=extxyz --q-sampling="line" --q-direction=10,0,0 --q-points=100 --q-bins=100

or, to conduct a spherical average over :math:`\boldsymbol{q}`-vectors, one can run ::

   dynasor dump.xyz --trajectory-format=extxyz --q-sampling="isotropic" --q-max=10 --q-bins=100 --max_q_points=20000

Time sampling options
---------------------

For example, the command ::

    dynasor ... --nt=500 --max-frames=2000

implies that correlation functions are calculated for frames separated by up to 500 frames.
This window is moved over the trajectory until it reaches frame number ``max-frames``.

The :ref:`examples section <examples>` contains more examples for how to use :program:`dynasor` via the command line.

.. index:: Output options

Output options
--------------
The output files can be written in the form of :program:`numpy` npy-files.
For example use as ::

   dynasor -f dump.atom --outfile=output.npy

These output functions can be easily read in :program:`python` using functions from the :class:`Sample` class or :program:`numpy` directly.

Self-part of the intermediate scattering function and velocity correlations
---------------------------------------------------------------------------

The self-part of the intermediate scattering function (incoherent part) is not evaluated by default but can be calculated by adding the :attr:`--calculate-self` option ::

    dynasor ... --calculate-self


Similarly the current correlation functions (velocity correlation functions) are calculated by ::

    dynasor ... --calculate-currents





