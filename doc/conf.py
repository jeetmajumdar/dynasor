# -*- coding: utf-8 -*-

import re
import sys
import os
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('../dynasor/'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',
    'sphinx.ext.napoleon',
    'sphinxcontrib.bibtex',
    'sphinx_autodoc_typehints',
    'sphinx.ext.viewcode',
    'sphinx_sitemap',
    'nbsphinx',
]


site_url = 'https://dynasor.materialsmodeling.org/'
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
pygments_style = 'sphinx'
todo_include_todos = True
bibtex_bibfiles = ['references.bib']

# Collect basic information from main module
with open('../dynasor/__init__.py') as fd:
    lines = '\n'.join(fd.readlines())
version = re.search("__version__ = '(.*)'", lines).group(1)  # noqa
release = ''
copyright = re.search("__copyright__ = '(.*)'", lines).group(1)  # noqa
project = re.search("__project__ = '(.*)'", lines).group(1)  # noqa
author = re.search("__maintainer__ = '(.*)'", lines).group(1)  # noqa

site_url = 'https://dynasor.materialsmodeling.org/'
html_logo = '_static/logo.png'
html_favicon = '_static/logo.ico'
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_static_path = ['_static']
html_theme_options = {'display_version': False}
html_context = {
    'current_version': version,
    'versions':
        [('latest stable release',
          '{}'.format(site_url)),
         ('development version',
          '{}/dev'.format(site_url))]}

htmlhelp_basename = 'dynasordoc'
intersphinx_mapping = {'ase':    ('https://wiki.fysik.dtu.dk/ase', None),
                       'python': ('https://docs.python.org/3.6', None)}


# Options for LaTeX output
_PREAMBLE = r"""
\usepackage{amsmath,amssymb}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\argmin}{\arg\!\min}
"""

latex_elements = {
    'preamble': _PREAMBLE,
}
latex_documents = [
    (master_doc, 'dynasor.tex', 'dynasor Documentation',
     'The dynasor developer team', 'manual'),
]


# Options for manual page output
man_pages = [
    (master_doc, 'dynasor', 'dynasor Documentation',
     [author], 1)
]


# Options for Texinfo output
texinfo_documents = [
    (master_doc, 'dynasor', 'dynasor Documentation',
     author, 'dynasor',
     'A tool for calculating dynamic structure factors',
     'Miscellaneous'),
]


html_css_files = [
    'custom.css',
]
