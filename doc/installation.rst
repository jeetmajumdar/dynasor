.. index:: Installation

Installation
************

Installation via ``pip`` or ``conda``
=====================================

Stable versions of :program:`dynasor` are provided via `PyPI <https://pypi.org/project/dynasor/>`_ and as part of `conda-forge <https://anaconda.org/conda-forge/dynasor/>`_.
This implies that :program:`dynasor` can be installed using ``pip`` via::

    pip install dynasor

or using ``conda`` via::

    conda install -c conda-forge dynasor


Installation via ``setup.py``
=============================
.. index:: Git repository

If installation via ``pip`` fails or if you want to use the most recent
(development) version you can clone the repository and install using the
``setup.py`` script as follows::
	   
   git clone git@gitlab.com:materials-modeling/dynasor.git

or in the form of a tarball or zip-archive. On nix
systems, compilation is usually staightforward after cloning the repository 
and merely requires executing the following commands on the command line while standing in the ``dynasor`` directory::

    pip install --user .

or::

    python3 setup.py install --user


Requirements
============

:program:`dynasor` requires :program:`Python` 3.6 (or higher) and depends on the following libraries

* `ASE <https://wiki.fysik.dtu.dk/ase>`_ (trajectory reading)
* `MDAnalysis <https://www.mdanalysis.org/>`_ (trajectory reading)
* `NumPy <http://www.numpy.org/>`_ (numerical linear algebra)
* `Numba <https://numba.pydata.org/>`_ (computational efficiency)


numba and icc_rt
================
:program:`dynasor` employs :program:`numba` for efficient calculations of correlation functions.
To get the full benefit of using :program:`numba`, `see here <https://numba.pydata.org/numba-doc/latest/user/performance-tips.html#intel-svml>`_ , you need to install :program:`icc_rt`, which can be installed using, e.g., ``conda`` or ``pip``.    
:program:`icc_rt` can lead to certain use cases running 5 to 10 times faster. 

Note that there is an existing bug when installing :program:`icc_rt` with ``pip``, see this `github issue <https://github.com/numba/numba/issues/4713>`_.
If you run ``numba -s`` the ideal output is::

    __SVML Information__
    SVML State, config.USING_SVML                 : True
    SVML Library Loaded                           : True
    llvmlite Using SVML Patched LLVM              : True
    SVML Operational                              : True

If any of these are ``False`` we recommend following the steps in the github issue linked above in order to install :program:`icc_rt`.