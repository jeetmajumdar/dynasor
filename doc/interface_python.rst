.. _interface_python:

Python interface
****************

Correlation functions
---------------------

The interface to :program:`dynasor` when computing structure factors consists of two functions, which are used for dynamic and static correlations, respectively.

* :func:`compute_correlation_functions_static_qiso <dynasor.correlation_functions.compute_dynamic_structure_factors>`
* :func:`compute_correlation_functions_dynamic_qiso <dynasor.correlation_functions.compute_static_structure_factors>`

:program:`dynasor` also provides functionality for computing the spectral energy density (SED) via the function :func:`compute_spectral_energy_density <dynasor.correlation_functions.compute_spectral_energy_density>` :cite:`Thomas2010`.


.. automodule:: dynasor.correlation_functions
   :members:
   :inherited-members:



Below some possibly useful functions are listed that can be used without running the core parts of dynasor.

.. index::
   single: Function reference; Trajectory handling

Trajectory handling
--------------------

.. automodule:: dynasor.trajectory
   :members:
   :inherited-members:


Sample
------
The resulting static and dynamic structure factors are stored in a Sample object.


.. automodule:: dynasor.sample
   :members:
   :inherited-members:

Post processing
---------------

.. automodule:: dynasor.post_processing
   :members:
   :inherited-members:

Tools
-----

.. automodule:: dynasor.tools.acfs
   :members:
   :inherited-members:


.. automodule:: dynasor.tools.damped_harmonic_oscillator
   :members:
   :inherited-members:


Logging
-------

.. automodule:: dynasor.logging_tools
   :members:
   :inherited-members: