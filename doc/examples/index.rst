.. _examples:

.. index:: examples

Examples
********

The ``examples`` folder on `gitlab
<https://gitlab.com/materials-modeling/dynasor>`_ contains serveral simple examples that
demonstate how to use :program:`dynasor`. Specifically, these examples
illustrate the analysis of an elemental liquid (:ref:`molten Al
<example_liquid_aluminum>`), an elemental crystal (:ref:`fcc Al
<example_crystalline_aluminum>`), as well as a compound (:ref:`molten
sodium chloride <example_sodium_chloride>`).

In setting up these examples, suitable parameters were chosen for the
MD simulations that provide physically sound (albeit not necessarily
highly converged) results. In practice, it is strongly recommended to
carefully test the effect of e.g., system size, sampling time, spacing
of snapshots, possibly potential/force field, and
:program:`dynasor` averaging parameters.

The MD simulations that are analyzed here were carried out using the
`lammps
<http://lammps.sandia.gov/>`_ code. The input files used to
generate the trajectroies are included for reference and can be found
in the `examples` directory.

There are also more extensive examples available in the repository `dynasor-examples <https://gitlab.com/materials-modeling/dynasor-examples>`_.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 1

   aluminum
   sodium_chloride
   sed
