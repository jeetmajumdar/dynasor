.. raw:: html

  <p>
  <a href="https://badge.fury.io/py/dynasor"><img src="https://badge.fury.io/py/dynasor.svg" alt="PyPI" height="18"></a>
  <a href="https://anaconda.org/conda-forge/dynasor/"><img src="https://anaconda.org/conda-forge/dynasor/badges/version.svg" alt="Conda" height="18"></a>
  </p>
  

:program:`dynasor` -- Correlations for everyone
***********************************************

:program:`dynasor` is a tool for calculating total and partial dynamic structure factors from molecular dynamics (MD) simulations.
The main input consists of a trajectory from a MD simulation, i.e., a file containing snapshots of the particle coordinates, and optionally velocities that correspond to consecutively and equally spaced points in (simulation) time.
The following snippet illustrates how one can calculate dynamic structure factors from a MD trajectory ::

   traj = Trajectory('dump.xyz', trajectory_format='extxyz')
   q_points = generate_spherical_qpoints(traj.cell, q_max=20)
   sample = compute_dynamic_structure_factors(traj, q_points=q_points, dt=5, window_size=100)
   sample.write_to_npy('test.npy')

:program:`dynasor` has been developed at Chalmers University of Technology in
Gothenburg, Sweden, in the
`Condensed Matter and Materials Theory division <http://www.materialsmodeling.org>`_
at the Department of Physics. Please consult the :ref:`credits page <credits>` for information on how to cite :program:`dynasor`.

For questions and help please use the `dynasor discussion forum on matsci.org <https://matsci.org/dynasor>`_. :program:`dynasor` and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/dynasor>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/dynasor/issues>`_. 

.. toctree::
   :maxdepth: 2
   :caption: Main

   introduction
   installation
   theory
   workflow
   performance
   credits

.. toctree::
   :maxdepth: 3
   :caption: Tutorials and examples
   
   tutorials/index
   examples/index

.. toctree::
   :maxdepth: 3
   :caption: Function reference
   
   interface_python
   interface_cmdline
   
.. toctree::
   :maxdepth: 2
   :caption: Backmatter   
   	      
   bibliography
   genindex


